import java.util.ArrayList;
import java.util.HashMap;
import org.neo4j.graphdb.Node;

/**
 * @author Palash Gandhi
 * @date 11/30/17. GraphDatabases
 */
public class AnalysisArchive {

  HashMap<Node, Integer> search_space_size;
  long search_space_time;
  ArrayList<HashMap<Node, Node>> solutions;
  long solution_time;

  AnalysisArchive() {
    search_space_size = new HashMap<>();
    search_space_time = 0;
    solutions = new ArrayList();
    solution_time = 0;
  }

  public String toString() {
    return "Search space time: " + search_space_time + "\nTime taken to find all solutions: "
        + solution_time;
  }
}
