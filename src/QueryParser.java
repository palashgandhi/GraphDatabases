import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 8/3/17 11:23 PM
 * CSCI729_Graph_Databases
 */
public class QueryParser {
    /**
     * The class that is used to parse the given format into a Cypher query.
     * */

    HashMap<String, String> nodes = new HashMap();
    HashMap<String, ArrayList<String>> relationships = new HashMap();
    HashMap<String, ArrayList<String>> constraints = new HashMap();
    HashMap<String, ArrayList<String>> condition_negate = new HashMap();
    String parse_input(ArrayList<String> input){
        /*
        * Method that parses the input and returns the query.
        * @param input: Arraylist containing each line of the input as an
        * element.
        * @returns cypher_query: The parsed Cypher query.
        * */
        String cypher_query = "";
        cypher_query += "MATCH ";
        String condition = " WHERE ";
        for(String line: input){
            String[] split_line = line.split(" ");
            if(!nodes.containsKey(split_line[1]) && !split_line[1].equals
                    ("<>")){
                nodes.put(split_line[0], split_line[1]);
                if(split_line.length>2) {
                    ArrayList<String> constraints_arr = new ArrayList<>();
                    for(int count = 2; count<split_line.length; count++){
                        constraints_arr.add(split_line[count]);
                    }
                    constraints.put(split_line[0], constraints_arr);
                }
            }
            else if(nodes.containsKey(split_line[1]) && !split_line[1].equals
                    ("<>")){
                ArrayList insert;
                if(relationships.containsKey(split_line[0])){
                    insert = relationships.get(split_line[0]);
                }
                else{
                    insert = new ArrayList<String>();
                }
                insert.add(split_line[1]);
                relationships.put(split_line[0], insert);
            }
            else if(split_line[1].equals("<>")){
                ArrayList insert;
                if(condition_negate.containsKey(split_line[0])){
                    insert = condition_negate.get(split_line[0]);
                }
                else{
                    insert = new ArrayList();
                }
                insert.add(split_line[2]);
                condition_negate.put(split_line[0], insert);
            }
        }
        int count_rel = 0;
        for(String node: relationships.keySet()){
            for(String neighbor: relationships.get(node)) {
                count_rel += 1;
            }
        }
        int count = 0;
        for(String node: relationships.keySet()){
            for(String neighbor: relationships.get(node)) {
                cypher_query += "(" + node + ":" + nodes.get(node) + ")--(" + neighbor + ":" + nodes.get(neighbor) + ")";
                count+=1;
                if (count < count_rel) {
                    cypher_query += ",";
                }
            }
        }
        for(String node1: condition_negate.keySet()){
            for(String node2: condition_negate.get(node1)) {
                condition += node1 + " <> " + node2;
                condition += " AND ";
            }
        }
        int count_nodes = 0;
        for(String node: constraints.keySet()){
            for(String constr: constraints.get(node)){
                count_nodes += 1;
                condition += node+"."+constr;
                if(count_nodes<constraints.size()+constraints.get(node).size
                        ()-1){
                    condition += " AND ";
                }
            }
        }
        cypher_query += condition;
        cypher_query += " RETURN ";
        count_nodes = 0;
        for(String node:nodes.keySet()){
            count_nodes += 1;
            cypher_query += node;
            if(count_nodes != nodes.size()) {
                 cypher_query += ",";
            }
        }
        return cypher_query;
    }

    public static void main(String[] args) {
        ArrayList<String> input = new ArrayList<>();
        System.out.println("Enter the query:");
        Scanner sc = new Scanner(System.in);
        String inp = "";
        while(true){
            String input_str = sc.nextLine();
            if(input_str.length()==0){
                break;
            }
            else{
                inp += input_str+"\n";
            }
        }

        String[] input_arr = inp.split("\n");
        for(String line: input_arr){
            input.add(line);
        }
        System.out.println("Parsing the input query...");
        QueryParser parser = new QueryParser();
        String query = parser.parse_input(input);
        System.out.println(query);
    }
}
