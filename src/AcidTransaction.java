/**
 * This module establishes a connection with the database and tries to insert
 * a row without errors and 1 with errors causing it to rollback if an
 * exception does occur.
 * It tries to insert a very large integer value and thus an entire rollback
 * of the transaction occurs.
 * Run instructions: java AcidTransactions
 * Created by Palash on 2/2/17.
 * Assignment 1: Question 6
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AcidTransaction {
    static String database = "imdb";
    static String url = "jdbc:mysql://localhost:3306/" + database +
            "?useSSL=false";
    static String username = "root";
    static String password = "palash15";
    static Connection connection;

    static void get_count() {
        /**
         * Used to fetch the total number of movies from the table movies.
         * */
        try {
            ResultSet rs = connection.createStatement().executeQuery("select" +
                    " " +
                    "COUNT(name) from movies");
            while (rs.next()) {
                System.out.println("Number of movies in database: " + rs.getInt
                        (1));
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }
    }

    static int get_max_id() {
        /**
         * Used to fetch the max id and return the new id where a new record
         * should be inserted. This is done only because the attribute id of
         * the table movies is not set to 'AUTO_INCREMENT'.
         * */
        try {
            ResultSet rs_max_id = connection.createStatement().executeQuery
                    ("SELECT MAX(id) FROM " +
                            "movies");
            while (rs_max_id.next()) {
                return rs_max_id.getInt(1) + 1;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return -1;
    }

    public static void main(String[] args) throws SQLException {
        try {
            int max_id;
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println("Connecting to database " + database);
            connection = DriverManager.getConnection(url, username, password);
            connection.setAutoCommit(false);
            max_id = get_max_id();
            get_count();
            System.out.println("Inserting test record " +
                    "without errors...");
            connection.createStatement().execute("ALTER TABLE movies " +
                    "ENGINE = InnoDB");
            connection.createStatement().executeUpdate("Insert into movies " +
                    "(id, name, year," +
                    " " +
                    "rank) " +
                    "VALUES (" + max_id + ",'Test', 2018, 299)");
            System.out.println("Inserting test record " +
                    "with error(large integer for attribute rank)...");
            max_id = get_max_id();
            connection.createStatement().executeUpdate("Insert into movies " +
                    "(id, name, year, rank) VALUES (" + max_id + ", 'Test', " +
                    "2017, " +
                    "299999999999999999999999999999999999999999999999999)");
            connection.commit();

        } catch (Exception e) {
            /**
             * Rollback if exception occurs.
             * */
            connection.rollback();
            System.out.println("Exception occurred. Not committing: " + e);
        } finally {
            get_count();
            connection.close();
        }

    }
}
