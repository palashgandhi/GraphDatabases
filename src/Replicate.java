import java.sql.*;

/**
 * This mocule is used to replicate the data from the old schema to the new
 * proposed schema. It will create a database called imdb_new_schema if it
 * does not already exist at the host. It will then create tables as per the
 * new schema if those tables do not already exist.
 * It then starts the replication of the data.
 * Run instructions: java Replicate
 * Created by Palash on 2/1/17.
 * Question 7
 */
public class Replicate {
    /**
     * Class that contains methods to replicate data from the old schema to
     * the new schema.
     */

    static String database = "imdb";
    static String url = "jdbc:mysql://localhost:3306/?useSSL=false";
    static String username = "root";
    static String password = "palash15";
    static Connection connection;
    static Statement stmt;

    static void create_database() {
        /**
         * This is the first method that is called which will create the
         * database at the host.
         * */
        try {
            System.out.println("Creating database imdb_new_schema if not " +
                    "exists.");
            stmt.executeUpdate("CREATE DATABASE IF NOT EXISTS imdb_new_schema");
            System.out.println("Selecting database imdb_new_schema");
            stmt.executeQuery("use imdb_new_schema");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    static void create_tables() {
        /**
         * This method is called after creating the database to create the
         * tables as per the required schema.
         * */
        try {
            System.out.println("Creating table Person if not exists.");
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Person (person_id " +
                    "INT(11) NOT NULL AUTO_INCREMENT," +
                    "name VARCHAR(100), gender CHAR(1), PRIMARY KEY" +
                    "(person_id))");
            System.out.println("Creating table Movie if not exists.");
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Movie (movie_id " +
                    "INT(11) NOT NULL AUTO_INCREMENT," +
                    "title VARCHAR(100) NOT NULL, release_year INT(11), " +
                    "PRIMARY KEY(movie_id))");
            System.out.println("Creating table Actor if not exists.");
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Actor (person_id " +
                    "INT(11) NOT NULL ," +
                    "movie_id INT(11) NOT NULL)");
            System.out.println("Creating table Director if not exists.");
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Director " +
                    "(person_id INT(11) NOT NULL ," +
                    "movie_id INT(11) NOT NULL)");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }
    }

    static void replicate() {
        /**
         * After the database and the tables are created, this method is
         * called to start pulling data from the old
         * schema and pushing it to the new schema that was just created. It
         * first replicates the movies and then the
         * actors using imdb.roles and then the directors using imdb.directors.
         * */
        System.out.println("Starting replication of all tables from old to " +
                "new schema.");
        replicate_movies();
        replicate_actors();
    }

    static void replicate_movies() {
        /**
         * This method fetches all the data from the table imdb.movies and
         * copies this data to imdb_new_schema.Movie.
         * */
        System.out.println("Starting replication of table movies to new " +
                "schema.");
        try {
            connection.createStatement().executeQuery("use imdb");
            ResultSet rs_movies = connection.createStatement().executeQuery
                    ("SELECT * FROM movies");
            connection.createStatement().executeQuery("use imdb_new_schema");
            while (rs_movies.next()) {
                String movie_name = rs_movies.getString(2);
                int year = rs_movies.getInt(3);
                String query = "INSERT INTO Movie (title, release_year) " +
                        "VALUES (?,?)";
                PreparedStatement ps = connection.prepareStatement(query);
                ps.setString(1, movie_name);
                ps.setInt(2, year);
                ps.execute();
                System.out.println(ps);
            }
            rs_movies.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }
    }

    static void replicate_actors() {
        /**
         * This method pulls data from the table imdb.actors and imdb.roles
         * and pushes this data to the new schema
         * imdb_new_schema.Person and imdb_new_schema.Actor.
         * */
        System.out.println("Starting replication of table actors to new " +
                "schema.");
        try {
            connection.createStatement().executeQuery("use imdb");
            ResultSet rs_actors = connection.createStatement().executeQuery
                    ("SELECT * FROM actors");
            connection.createStatement().executeQuery("use imdb_new_schema");
            while (rs_actors.next()) {
                int actor_id_old_schema = rs_actors.getInt(1);
                String actor_first_name = rs_actors.getString(2);
                String actor_last_name = rs_actors.getString(3);
                char gender = rs_actors.getString(4).charAt(0);
                String query = "INSERT INTO Person (name, gender) VALUES (?,?)";
                PreparedStatement ps = connection.prepareStatement(query);
                ps.setString(1, actor_first_name + " " + actor_last_name);
                ps.setString(2, gender + "");
                ps.execute();
                ResultSet rs_id = connection.createStatement().executeQuery
                        ("SELECT LAST_INSERT_ID()");
                int last_inserted_id = -1;
                while (rs_id.next()) {
                    last_inserted_id = rs_id.getInt(1);
                }
                rs_id.close();
                connection.createStatement().executeQuery("use imdb");
                PreparedStatement ps_roles = connection.prepareStatement
                        ("SELECT * FROM roles WHERE actor_id=?");
                ps_roles.setInt(1, actor_id_old_schema);
                ResultSet rs_roles = ps_roles.executeQuery();
                while (rs_roles.next()) {
                    int movie_id_old = rs_roles.getInt(2);
                    connection.createStatement().executeQuery("use imdb");
                    PreparedStatement ps_movies_roles = connection
                            .prepareStatement("SELECT * FROM movies WHERE " +
                                    "id=?");
                    ps_movies_roles.setInt(1, movie_id_old);
                    ResultSet rs_movie_actor = ps_movies_roles.executeQuery();
                    while (rs_movie_actor.next()) {
                        String movie_name = rs_movie_actor.getString(2);
                        int year = rs_movie_actor.getInt(3);
                        connection.createStatement().executeQuery("use " +
                                "imdb_new_schema");
                        PreparedStatement ps_movies_new = connection
                                .prepareStatement("SELECT * FROM Movie WHERE " +
                                        "title=? AND release_year=?");
                        ps_movies_new.setString(1, movie_name);
                        ps_movies_new.setInt(2, year);
                        ResultSet rs_movie_id_new = ps_movies_new
                                .executeQuery();
                        while (rs_movie_id_new.next()) {
                            int movie_id = rs_movie_id_new.getInt(1);
                            PreparedStatement ps_insert_actors = connection
                                    .prepareStatement("INSERT INTO Actor " +
                                            "(person_id, movie_id) VALUES (?," +
                                            " ?)");
                            ps_insert_actors.setInt(1, last_inserted_id);
                            ps_insert_actors.setInt(2, movie_id);
                            ps_insert_actors.executeUpdate();
                        }
                        rs_movie_id_new.close();
                    }
                }
            }
            rs_actors.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }
        System.out.println("Completed replication of table actors to new " +
                "schema.");
    }

    static void replicate_directors() {
        /**
         * This method pulls data from the table imdb.directors and imdb
         * .movies_directors and pushes this data to the
         * new schema imdb_new_schema.Person and imdb_new_schema.Director.
         * */
        System.out.println("Starting replication of table directors to new " +
                "schema.");
        try {
            connection.createStatement().executeQuery("use imdb");
            ResultSet rs_directors = connection.createStatement()
                    .executeQuery("SELECT * FROM directors");
            connection.createStatement().executeQuery("use imdb_new_schema");
            while (rs_directors.next()) {
                int director_id_old_schema = rs_directors.getInt(1);
                String director_first_name = rs_directors.getString(2);
                String director_last_name = rs_directors.getString(3);
                char gender = rs_directors.getString(4).charAt(0);
                String query = "INSERT INTO Person (name, gender) VALUES (?,?)";
                PreparedStatement ps = connection.prepareStatement(query);
                ps.setString(1, director_first_name + " " + director_last_name);
                ps.setString(2, gender + "");
                ps.execute();
                ResultSet rs_id = connection.createStatement().executeQuery
                        ("SELECT LAST_INSERT_ID()");
                int last_inserted_id = -1;
                while (rs_id.next()) {
                    last_inserted_id = rs_id.getInt(1);
                }
                rs_id.close();
                connection.createStatement().executeQuery("use imdb");
                PreparedStatement ps_roles = connection.prepareStatement
                        ("SELECT * FROM movies_directors WHERE director_id=?");
                ps_roles.setInt(1, director_id_old_schema);
                ResultSet rs_roles = ps_roles.executeQuery();
                while (rs_roles.next()) {
                    int movie_id_old = rs_roles.getInt(2);
                    connection.createStatement().executeQuery("use imdb");
                    PreparedStatement ps_movies_roles = connection
                            .prepareStatement("SELECT * FROM movies WHERE " +
                                    "id=?");
                    ps_movies_roles.setInt(1, movie_id_old);
                    ResultSet rs_movie_director = ps_movies_roles
                            .executeQuery();
                    while (rs_movie_director.next()) {
                        String movie_name = rs_movie_director.getString(2);
                        int year = rs_movie_director.getInt(3);
                        connection.createStatement().executeQuery("use " +
                                "imdb_new_schema");
                        PreparedStatement ps_movies_new = connection
                                .prepareStatement("SELECT * FROM Movie WHERE " +
                                        "title=? AND release_year=?");
                        ps_movies_new.setString(1, movie_name);
                        ps_movies_new.setInt(2, year);
                        ResultSet rs_movie_id_new = ps_movies_new
                                .executeQuery();
                        while (rs_movie_id_new.next()) {
                            int movie_id = rs_movie_id_new.getInt(1);
                            PreparedStatement ps_insert_directors =
                                    connection.prepareStatement("INSERT INTO " +
                                            "Director (person_id, movie_id) " +
                                            "VALUES (?, ?)");
                            ps_insert_directors.setInt(1, last_inserted_id);
                            ps_insert_directors.setInt(2, movie_id);
                            ps_insert_directors.executeUpdate();
                        }
                        rs_movie_id_new.close();
                    }
                }
            }
            rs_directors.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }
        System.out.println("Completed replication of table directors to new " +
                "schema.");
    }

    public static void main(String[] args) {
        /**
         * Main method that makes the connection to the host and the database
         * to start replication.
         */
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println("Connecting to host " + url);
            connection = DriverManager.getConnection(url, username, password);
            connection.setAutoCommit(false);
            stmt = connection.createStatement();
            create_database();
            create_tables();
            replicate();
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }
    }
}
