import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.unsafe.batchinsert.BatchInserter;
import org.neo4j.unsafe.batchinsert.BatchInserters;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 4/3/17 1:31 PM
 * CSCI729_Graph_Databases
 */
public class GraphMigrate {
    /**
     * This class is used to migrate the mysql database into the neo4j database.
     * This is done mainly using mysql queries and the interface
     * BatchInserters to insert into neo4j.
     */
    static String limit = "  ";

    static String url_mysql = "jdbc:mysql://localhost:3306/?useSSL=false";
    static String username_mysql = "root";
    static String password_mysql = "palash15";
    static Connection connection_mysql;
    static Statement stmt_mysql;

    BatchInserter bi;

    HashMap<Integer, Long> movies_nodes = new HashMap<>();
    HashMap<Integer, Long> actors_nodes = new HashMap<>();
    HashMap<Integer, Long> directors_nodes = new HashMap<>();
    HashMap<String, Long> genre_nodes = new HashMap<>();

    GraphMigrate() {
        /*
        * Constructor for this class. Connection to the mysql database and
        * the neo4j database is established when an object is initialized of
        * this class. If the neo4j database does not exist in the location
        * entered, then a new database is created. If it exists,
        * */
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connection_mysql = DriverManager.getConnection(url_mysql,
                    username_mysql, password_mysql);
            stmt_mysql = connection_mysql.createStatement();
            connection_mysql.createStatement().executeQuery("use imdb");
            File db_file = new File
                    ("/home/palash/Desktop/neo4j-community-3.1.1/data" +
                            "/databases/imdb");
            if (db_file.exists()) {
                db_file.delete();
            }
            bi = BatchInserters.inserter(db_file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        System.out.println(new Date() + " Extracting and creating nodes " +
                "simultaneously...");
        GraphMigrate migrate = new GraphMigrate();
        migrate.extract();
        System.out.println(new Date() + " Transforming and loading...");
        migrate.load();
        System.out.println(new Date() + " Migrate finished");
        migrate.shutdown();
    }

    static ResultSet execute_mysql_query(String query) throws java.sql
            .SQLException {
        /*
        * Helper function that executes a mysql query and returns the ResultSet.
        * */
        return connection_mysql.createStatement().executeQuery(query + limit);
    }

    void extract() {
        /*
        * Method that is called to begin the extraction and create nodes
        * simultaneously.
        * */
        extract_movies_and_create_nodes();
        extract_actors_and_create_nodes();
        extract_directors_and_create_nodes();
        extract_genres_and_create_nodes();
    }

    void extract_movies_and_create_nodes() {
        /*
        * Method that extracts all movies from the mysql database and creates
         * nodes in neo4j using the BatchInserter object. It also stores the
         * node ids into a local hashmap mapping the movie id to the node id
         * that just got created for that movie.
        * */
        System.out.println(new Date() + " Extracting movies and creating " +
                "nodes...");
        try {
            ResultSet rs = execute_mysql_query("SELECT * FROM movies");
            while (rs.next()) {
                HashMap properties = new HashMap();
                properties.put("name", rs.getString(2));
                properties.put("year", rs.getInt(3));
                properties.put("rank", rs.getDouble(4));
                long movie_node = bi.createNode(properties, Label.label
                        ("MOVIE"));
                properties.clear();
                movies_nodes.put(rs.getInt(1), movie_node);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void extract_actors_and_create_nodes() {
        /*
        * Method that extracts all actors from the mysql database and creates
         * nodes in neo4j using the BatchInserter object. It also stores the
         * node ids into a local hashmap mapping the actor id to the node id
         * that just got created for that actor.
        * */
        System.out.println(new Date() + " Extracting actors and creating " +
                "nodes...");
        try {
            ResultSet rs = execute_mysql_query("SELECT * FROM actors");
            while (rs.next()) {
                HashMap properties = new HashMap();
                properties.put("first_name", rs.getString(2));
                properties.put("last_name", rs.getString(3));
                long actor_node = bi.createNode(properties, Label.label
                        ("ACTOR"));
                properties.clear();
                actors_nodes.put(rs.getInt(1), actor_node);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void extract_directors_and_create_nodes() {
        /*
        * Method that extracts all directors from the mysql database and creates
         * nodes in neo4j using the BatchInserter object. It also stores the
         * node ids into a local hashmap mapping the director id to the node id
         * that just got created for that director.
        * */
        System.out.println(new Date() + " Extracting directors and creating " +
                "nodes...");
        try {
            ResultSet rs = execute_mysql_query("SELECT * FROM directors");
            while (rs.next()) {
                HashMap properties = new HashMap();
                properties.put("first_name", rs.getString(2));
                properties.put("last_name", rs.getString(3));
                long director_node = bi.createNode(properties, Label
                        .label("DIRECTOR"));
                properties.clear();
                directors_nodes.put(rs.getInt(1), director_node);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void extract_genres_and_create_nodes() {
        /*
        * Method that extracts all genres from the mysql database and creates
         * nodes in neo4j using the BatchInserter object. It also stores the
         * node ids into a local hashmap mapping the genre to the node id
         * that just got created for that genre.
        * */
        System.out.println(new Date() + " Extracting genres and creating " +
                "nodes...");
        try {
            ResultSet rs = execute_mysql_query("SELECT DISTINCT(genre) FROM " +
                    "movies_genres");
            while (rs.next()) {
                String genre = rs.getString(1);
                HashMap properties = new HashMap();
                properties.put("name", genre);
                long genre_node = bi.createNode(properties, Label.label
                        ("GENRE"));
                genre_nodes.put(genre, genre_node);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void load() {
        /*
        * Function that begins creating relationships between the
        * created nodes.
        * */
        relate_movies_and_directors();
        relate_movies_and_genres();
        relate_actors_and_movies();
        relate_directors_and_genres();
    }

    void relate_movies_and_directors() {
        /*
        * Method that relates movie nodes to directors with the relationship
        * 'DIRECTED_BY'.
        * */
        System.out.println(new Date() + " 1/4 Establishing relationships " +
                "between movies and directors...");
        try {
            ResultSet rs = execute_mysql_query("SELECT m.id, md.director_id " +
                    "FROM movies as m INNER JOIN movies_directors as md " +
                    "ON m.id=md.movie_id");
            while (rs.next()) {
                if (directors_nodes.containsKey(rs.getInt(2)) &&
                        movies_nodes.containsKey(rs.getInt(1))) {
                    bi.createRelationship(directors_nodes.get(rs.getInt(2)),
                            movies_nodes.get(rs.getInt(1)),
                            RelationshipType.withName("DIRECTED_BY"),
                            null);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    void relate_movies_and_genres() {
        /*
        * Method that relates movie nodes to genres with the relationship
        * 'MOVIE_GENRE'.
        * */
        System.out.println(new Date() + " 2/4 Establishing relationships " +
                "between movies and genres...");
        try {
            ResultSet rs = execute_mysql_query("SELECT m.id,mg.genre FROM " +
                    "movies as m INNER JOIN movies_genres as mg ON mg" +
                    ".movie_id=m.id");
            while (rs.next()) {
                String genre = rs.getString(2);
                if (genre_nodes.containsKey(genre)) {
                    bi.createRelationship(movies_nodes.get(rs.getInt(1)),
                            genre_nodes.get(genre),
                            RelationshipType.withName("MOVIE_GENRE"),
                            null);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void relate_actors_and_movies() {
        /*
        * Method that relates movie nodes to actors with the relationship
        * 'ACTED_IN'.
        * */
        System.out.println(new Date() + " 3/4 Establishing relationships " +
                "between movies and actors...");
        try {
            ResultSet rs = execute_mysql_query("SELECT m.id, r.actor_id, r" +
                    ".role FROM movies as m INNER JOIN roles as r ON m.id=r" +
                    ".movie_id");
            while (rs.next()) {
                int movie_id = rs.getInt(1);
                int actor_id = rs.getInt(2);
                String role = rs.getString(3);
                if (actors_nodes.containsKey(actor_id)) {
                    HashMap properties = new HashMap();
                    properties.put("role", role);
                    bi.createRelationship(actors_nodes.get(actor_id),
                            movies_nodes.get(movie_id), RelationshipType
                                    .withName("ACTED_IN"), properties);
                    properties.clear();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        movies_nodes.clear();
        actors_nodes.clear();
    }

    void relate_directors_and_genres() {
        /*
        * Method that relates director nodes to genres with the relationship
        * 'DIRECTOR_GENRE'.
        * */
        System.out.println(new Date() + " 4/4 Establishing relationships " +
                "between directors " +
                "and " +
                "genres...");

        try {
            ResultSet rs = execute_mysql_query("SELECT d.id, dg.genre" +
                    " FROM directors_genres as dg INNER JOIN directors as d " +
                    "ON dg.director_id=d.id");
            while (rs.next()) {
                int dir_id = rs.getInt(1);
                String genre = rs.getString(2);
                if (genre_nodes.containsKey(genre)) {
                    bi.createRelationship(directors_nodes.get
                                    (dir_id), genre_nodes.get(genre),
                            RelationshipType.withName("DIRECTOR_GENRE"),
                            null);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void shutdown() {
        /*
        * Used to shutdown the BatchInserter object and clear used hashmaps
        * to speed up garbage collection.
        * */
        System.out.println("Shutting down BatchInserter");
        bi.shutdown();
        try {
            connection_mysql.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        directors_nodes.clear();
        genre_nodes.clear();
    }
}
