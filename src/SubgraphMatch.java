import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.ResourceIterable;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.unsafe.batchinsert.BatchInserter;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 6/3/17 9:26 PM
 * CSCI729_Graph_Databases
 */
public class SubgraphMatch {

    BatchInserter bi;
    HashMap<Long, Map<String, Object>> movie_nodes;
    HashMap<Long, Map<String, Object>> director_nodes;
    HashMap<Long, Map<String, Object>> actor_nodes;
    HashMap<Long, Map<String, Object>> genre_nodes;
    HashMap<Long, Long> relationships;

    HashMap<String, HashMap<Long, Map<String, Object>>> adjacency_lists;

    static HashMap<String, String> nodes_parsed;
    static HashMap<String, ArrayList<String>> relationships_parsed;
    GraphDatabaseService db;

    SubgraphMatch() {
        movie_nodes = new HashMap<>();
        director_nodes = new HashMap<>();
        actor_nodes = new HashMap<>();
        genre_nodes = new HashMap<>();
        relationships = new HashMap<>();
        adjacency_lists = new HashMap<>();
        db = new GraphDatabaseFactory().newEmbeddedDatabase(new File
                ("/home/palash/Desktop/neo4j-community-3.1.1/data" +
                        "/databases/imdb"));
    }

    public static void main(String[] args) {

        ArrayList<String> input = new ArrayList<>();
        System.out.println("Enter the query:");
        Scanner sc = new Scanner(System.in);
        String inp = "";
        while(true){
            String input_str = sc.nextLine();
            if(input_str.length()==0){
                break;
            }
            else{
                inp += input_str+"\n";
            }
        }

        String[] input_arr = inp.split("\n");
        for(String line: input_arr){
            input.add(line);
        }
        System.out.println("Parsing the input query...");
        QueryParser parser = new QueryParser();
        parser.parse_input(input);
        nodes_parsed = parser.nodes;
        relationships_parsed = parser.relationships;

        SubgraphMatch sm = new SubgraphMatch();
        sm.get_nodes();
        sm.build_adjacency_list(nodes_parsed);
        sm.matching_order();
    }

    void build_adjacency_list(HashMap<String, String> nodes){
        System.out.println("Building adjacency list for all nodes...");
        for(String node: nodes.keySet()){
            String label = nodes.get(node);
            HashMap graph_nodes = new HashMap();
            if(label.equals("MOVIE")){
                graph_nodes = movie_nodes;
            }else if(label.equals("DIRECTOR")){
                graph_nodes = director_nodes;
            }else if(label.equals("ACTOR")){
                graph_nodes = actor_nodes;
            }else if(label.equals("GENRE")){
                graph_nodes = genre_nodes;
            }
            adjacency_lists.put(node, graph_nodes);
        }
    }

    void matching_order(){
        System.out.println("Building a matching order in a linked list");
        ArrayList order = new ArrayList();
        for(String node_alias: relationships_parsed.keySet()){
            if(order.size()>1 && !order.get(order.size() - 1).equals
                    (node_alias)){
                order.add(node_alias);
            }else if(order.size()==0){
                order.add(node_alias);
            }
            order.add(node_alias);
            String node_label = nodes_parsed.get(node_alias);
            for(long node_id: relationships.keySet()){
                Transaction tx = db.beginTx();
                if(db.getNodeById(node_id).hasLabel(Label.label(node_label))){
                    Iterable<Label> labels =db.getNodeById(relationships.get
                            (node_id))
                            .getLabels();
                    for(Label label: labels){
                        for(String neighbor: relationships_parsed.get(node_alias)){
                            if(nodes_parsed.get(neighbor).equals(label.name())){
                                if(!order.get(order.size() - 1).
                                        equals(neighbor)) {
                                    order.add(neighbor);
                                }
                            }
                        }
                    }
                }
                tx.close();
            }
        }
        System.out.println(order.toString());
    }

    void get_nodes() {
        System.out.println("Getting all nodes and relationships...");
        Transaction tx = db.beginTx();
        ResourceIterable<Node> nodes = db.getAllNodes();
        for(Node node: nodes){
            long node_id = node.getId();
            Map<String, Object> properties = node.getAllProperties();
            String label = get_label(node);
            save_node(node_id, properties, label);
            get_relationships(node);
        }
        System.out.println("FINISHED");
        System.out.println("Fetched "+movie_nodes.size()+" movie nodes.");
        System.out.println("Fetched "+director_nodes.size()+" director nodes.");
        System.out.println("Fetched "+actor_nodes.size()+" actor nodes.");
        System.out.println("Fetched "+genre_nodes.size()+" genre nodes.");
        System.out.println("Fetched "+relationships.size()+" relationships.");
        tx.close();
    }

    String get_label(Node node){
        Iterable<Label> labels = node.getLabels();
        for(Label label: labels){
            return label.name();
        }
        return "";
    }

    void save_node(long node_id, Map<String, Object> properties, String label){
        if(label.equals("MOVIE")){
            movie_nodes.put(node_id, properties);
        }
        else if(label.equals("DIRECTOR")){
            director_nodes.put(node_id, properties);
        }
        else if(label.equals("ACTOR")){
            actor_nodes.put(node_id, properties);
        }
        else if(label.equals("GENRE")){
            genre_nodes.put(node_id, properties);
        }
    }

    void get_relationships(Node node){
        Transaction tx = db.beginTx();
        Iterable<Relationship> relationships_iter = node.getRelationships
                (Direction.OUTGOING);
        for(Relationship relationship : relationships_iter){
            long end_node_id = relationship.getEndNode().getId();
            relationships.put(node.getId(), end_node_id);
        }
        tx.close();
    }
}
