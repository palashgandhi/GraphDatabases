import java.util.ArrayList;
import org.neo4j.graphdb.Node;

/**
 * Represents the state used by VF2 Plus.
 * @author Palash Gandhi
 * @date 12/13/17. GraphDatabases
 */
class State {

  ArrayList<Node> list_m1;
  ArrayList<Node> list_t1;
  ArrayList<Node> list_m2;
  ArrayList<Node> list_t2;

  State() {
    list_m1 = new ArrayList<>();
    list_t1 = new ArrayList<>();
    list_m2 = new ArrayList<>();
    list_t2 = new ArrayList<>();
  }

  public String toString() {
    return "M1: " + list_m1 + "\nM2: " + list_m2 + "\nT1: " + list_t1 + "\nT2: " + list_t2;
  }
}