import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;

/**
 * This class implements the VF2 Plus algorithm for subgraph isomorphism. It takes a neo4j query
 * database and a neo4j target database and finds all solutions.
 *
 * @author Palash Gandhi
 * @date 10/31/17. GraphDatabases
 */
public class VF2Plus {

  GraphDatabaseService query_db;
  GraphDatabaseService target_db;
  String query_file = "";
  String target_file = "";
  String ground_truth_file_path = "";
  DatabaseHelpers helper;

  HashMap<Node, ArrayList<Node>> search_space;
  HashMap<Node, Double> probabilities;
  ArrayList<Node> order;

  AnalysisArchive archive;


  /**
   * Constructor for class that sets the required fields. Takes the query file path and target file
   * path from which the databases are created if needed, or reused if they already exist, the
   * ground truth file which can be left blank but is used for verification of the solutions and the
   * GraphDatabaseService objects pointing to the query and target neo4j databases.
   *
   * @param query_file_path Query file from which the query database is created if needed, or reused
   * if the database already exists.
   * @param target_file_path Target file from which the query database is created if needed, or
   * reused if the database already exists.
   * @param ground_truth_file_path Ground truth file path which can be left blank but is used for
   * verification of the solutions
   * @param query_database org.neo4j.graphdb.GraphDatabaseService object for query database.
   * @param target_database org.neo4j.graphdb.GraphDatabaseService object for target database.
   */
  VF2Plus(String query_file_path, String target_file_path, String ground_truth_file_path,
      GraphDatabaseService query_database, GraphDatabaseService target_database) {
    query_file = query_file_path;
    target_file = target_file_path;
    this.ground_truth_file_path = ground_truth_file_path;
    query_db = query_database;
    target_db = target_database;
    helper = new DatabaseHelpers();
    search_space = new HashMap<>();
    probabilities = new HashMap<>();
    archive = new AnalysisArchive();
  }

  /**
   * Main routine to be called that makes use of all VF2 Plus methods to find solutions.
   */
  public AnalysisArchive match() {
    Transaction tx_query = query_db.beginTx();
    Transaction tx_target = target_db.beginTx();

    long start_time = System.currentTimeMillis();
    compute_search_space();
    archive.search_space_time = System.currentTimeMillis() - start_time;

    compute_probabilities();

    order = compute_order();

    State state = new State();

    start_time = System.currentTimeMillis();
    backtrack_search(0, new HashMap<>(), state);
    archive.solution_time = System.currentTimeMillis() - start_time;

    tx_query.close();
    tx_target.close();

    return archive;
  }

  /**
   * Computes the search space.
   */
  public void compute_search_space() {
    for (Node query_node : query_db.getAllNodes()) {
      ArrayList<Node> possible_matches_based_on_lables = helper
          .find_possible_matches(query_node, target_db);
      possible_matches_based_on_lables.removeIf(s -> s.getDegree() < query_node.getDegree());
      search_space.put(query_node, possible_matches_based_on_lables);
    }
  }

  /**
   * Computes priori probabilities to determine the matching order.
   */
  public void compute_probabilities() {
    for (Node query_node : search_space.keySet()) {
      double probability_of_finding_node_with_label =
          helper.find_possible_matches(query_node, target_db).size() * 1.0 / helper
              .get_all_nodes(target_db).size() * 1.0;
      double probability_of_finding_node_with_higher_degree =
          helper.get_all_nodes_with_degree_greater_than_or_equal(query_node.getDegree(), target_db)
              .size() * 1.0 / helper.get_all_nodes(target_db).size() * 1.0;
      double probability =
          probability_of_finding_node_with_label * probability_of_finding_node_with_higher_degree;
      probabilities.put(query_node, probability);
    }
  }

  /**
   * Computes the search order based on the computed probabilities.
   */
  public ArrayList<Node> compute_order() {
    ArrayList<Node> listM = new ArrayList<>();
    ArrayList<Node> listT = new ArrayList<>();
    Entry<Node, Double> min = null;
    for (Entry<Node, Double> entry : probabilities.entrySet()) {
      if (min == null) {
        min = entry;
      } else if (min.getValue() > entry.getValue()) {
        min = entry;
      } else if (min.getValue() == entry.getValue()) {
        if (min.getKey().getDegree() > entry.getKey().getDegree()) {
          min = entry;
        }
      }
    }
    Node selected = min.getKey();
    while (selected != null) {
      listM.add(selected);
      for (Node t : helper.get_neighbors(selected, query_db, Direction.BOTH)) {
        if (!listT.contains(t) && !listM.contains(t)) {
          listT.add(t);
        }
      }
      int max_degreeM = Integer.MIN_VALUE;
      for (Node next : listT) {
        int degreeM = calculate_degreeM(next, listM);
        if (degreeM > max_degreeM) {
          selected = next;
          max_degreeM = degreeM;
        } else if (degreeM == max_degreeM) {
          if (probabilities.get(selected) > probabilities.get(next)) {
            selected = next;
          }
        }
      }
      if (max_degreeM == Integer.MIN_VALUE) {
        selected = null;
      } else {
        listT.remove(selected);
      }
    }
    return listM;
  }

  /**
   * Main recursive subroutine to perform the backtracking search using the computed search space
   * and order.
   *
   * @param i Current index of the order.
   * @param solution Current mapping.
   * @param state State object representing the state.
   */
  void backtrack_search(int i, HashMap<Node, Node> solution, State state) {
    Transaction tx_query = query_db.beginTx();
    Transaction tx_target = target_db.beginTx();
    if (solution.size() == search_space.keySet().size()) {
      archive.solutions.add(solution);
      System.out.println("Solution found: " + solution);
      if (query_file.length() > 0 && target_file.length() > 0) {
        File query_file_obj = new File(query_file);
        File target_file_obj = new File(target_file);
        helper.compare_solution_and_ground_truth(ground_truth_file_path, solution,
            query_file_obj.getName(), target_file_obj.getName());
      }
    } else {
      Node u = order.get(i);
      for (Node v : search_space.get(u)) {
        if (!solution.values().contains(v) && can_map(u, v, solution) && look_ahead_rule(state, u,
            v)) {
          solution.put(u, v);

          state.list_m1.add(u);
          state.list_m2.add(state.list_m1.indexOf(u), v);

          state.list_t1.remove(u);
          for (Node m : state.list_m1) {
            for (Node t : helper.get_neighbors(m, query_db, Direction.BOTH)) {
              if (!state.list_t1.contains(t) && !state.list_m1.contains(t)) {
                state.list_t1.add(t);
              }
            }
          }

          state.list_t2.remove(v);
          for (Node n : state.list_m2) {
            for (Node t : helper.get_neighbors(n, target_db, Direction.BOTH)) {
              if (!state.list_t2.contains(t) && !state.list_m2.contains(t)) {
                state.list_t2.add(t);
              }
            }
          }

          backtrack_search(i + 1, solution, state);

          solution.remove(u);

          state.list_m1.remove(u);
          state.list_m2.remove(v);
          state.list_t1.clear();
          state.list_t2.clear();
        }
      }
    }
    tx_query.close();
    tx_target.close();
  }

  /**
   * Applies the VF2 Plus look-ahead rules which determine if a data node can be mapped to a query
   * node and prunes out the inconsistent states early.
   *
   * @param state State object representing the current state.
   * @param u Current query vertex in consideration.
   * @param v Current data vertex in consideration.
   * @return boolean
   */
  private boolean look_ahead_rule(State state, Node u, Node v) {
    if (r_term(state, u, v) && r_new(state, u, v)) {
      return true;
    }
    return false;
  }

  /**
   * Part of the look-ahead rules that the number of neighbors of the query node are less than or
   * equal to the number of neighbors of the target node.
   *
   * @param state State object representing the current state.
   * @param u Current query node in consideration.
   * @param v Current target node in consideration.
   * @return boolean
   */
  private boolean r_term(State state, Node u, Node v) {
    ArrayList<Node> adj_u = helper.get_neighbors(u, query_db, Direction.BOTH);
    ArrayList<Node> adj_v = helper.get_neighbors(v, target_db, Direction.BOTH);
    adj_u.retainAll(state.list_t1);
    adj_v.retainAll(state.list_t2);
    if (adj_u.size() <= adj_v.size()) {
      return true;
    }
    return false;
  }

  /**
   * Part of the look-ahead rules which checks if the current nodes in consideration lead to any
   * inconsistent states by checking neighborhood information.
   *
   * @param state State object representing the current state.
   * @param u Current query node in consideration.
   * @param v Current target node in consideration.
   * @return boolean
   */
  private boolean r_new(State state, Node u, Node v) {
    ArrayList<Node> adj_u = helper.get_neighbors(u, query_db, Direction.BOTH);
    ArrayList<Node> adj_v = helper.get_neighbors(v, target_db, Direction.BOTH);
    ArrayList<Node> n1_tilda = new ArrayList<>();
    ArrayList<Node> n2_tilda = new ArrayList<>();
    for (Node n : state.list_m1) {
      for (Node neighbor : helper.get_neighbors(n, query_db, Direction.BOTH)) {
        if (!state.list_m1.contains(neighbor)) {
          n1_tilda.add(neighbor);
        }
      }
    }
    for (Node n : state.list_m2) {
      for (Node neighbor : helper.get_neighbors(n, target_db, Direction.BOTH)) {
        if (!state.list_m2.contains(neighbor)) {
          n2_tilda.add(neighbor);
        }
      }
    }
    adj_u.retainAll(n1_tilda);
    adj_v.retainAll(n2_tilda);
    if (adj_u.size() <= adj_v.size()) {
      return true;
    }
    return false;
  }

  /**
   * Determines if a query node and target node mapping can be established.
   *
   * @param u Current query node in consideration.
   * @param v Current target node in consideration.
   * @param solution_param Current mapping.
   * @return boolean
   */
  boolean can_map(Node u, Node v, HashMap<Node, Node> solution_param) {
    /**
     * Check function that checks an edge from the query and target
     * graphs.
     * */
    boolean res = true;
    for (Node query_node : search_space.keySet()) {
      if (query_node.getId() == u.getId()) {
        continue;
      }
      if (solution_param.containsKey(query_node)) {
        for (Relationship rel : query_node.getRelationships()) {
          if (rel.getOtherNode(query_node).getId() == u.getId()) {
            boolean exists = false;
            for (Relationship rel2 : v.getRelationships()) {
              if (rel2.getOtherNode(v).getId() == solution_param.get(query_node).getId()) {
                exists = true;
                break;
              }
            }
            res = exists;
            if (!res) {
              return false;
            }
          }
        }
      }
    }
    return res;
  }


  /**
   * Calculates the value of degreeM which is the number of nodes that the node next is connected to
   * in listM.
   *
   * @param next Current node in consideration.
   * @param listM List of nodes.
   * @return degreeM of type int.
   */
  private int calculate_degreeM(Node next, ArrayList<Node> listM) {
    int degreeM = 0;
    ArrayList<Node> neighbors = helper.get_neighbors(next, query_db, Direction.BOTH);
    for (Node n : listM) {
      if (neighbors.contains(n)) {
        degreeM += 1;
      }
    }
    return degreeM;
  }

  public static void main(String[] args) {
    String query_file_path = "";
    String target_file_path = "";
    String ground_truth_file_path = "";
    GraphDatabaseService query_db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(
        new File("/home/pbg4930/Desktop/neo4j-community-3.1.1/data/databases/proteins.89.9.txt")).
        setConfig(GraphDatabaseSettings.pagecache_memory, "512M").
        setConfig(GraphDatabaseSettings.string_block_size, "60").
        setConfig(GraphDatabaseSettings.array_block_size, "300").
        newGraphDatabase();
    GraphDatabaseService target_db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(
        new File("/home/pbg4930/Desktop/neo4j-community-3.1.1/data/databases/proteins.99.txt")).
        setConfig(GraphDatabaseSettings.pagecache_memory, "512M").
        setConfig(GraphDatabaseSettings.string_block_size, "60").
        setConfig(GraphDatabaseSettings.array_block_size, "300").
        newGraphDatabase();

//    query_file_path = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/query/backbones_1EMA.8.sub.grf";
//    target_file_path = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/target/backbones_1EMA.grf";
//    ground_truth_file_path = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/ground_truth/Proteins.8.gtr";
//    FileDatabaseService file_helper = new FileDatabaseService(
//        "/home/pbg4930/Desktop/neo4j-community-3.1.1/data/databases/");
//    query_db = file_helper.create_db(query_file_path);
//    target_db = file_helper.create_db(target_file_path);

    VF2Plus matcher = new VF2Plus(query_file_path, target_file_path, ground_truth_file_path,
        query_db, target_db);
    matcher.match();
  }
}
