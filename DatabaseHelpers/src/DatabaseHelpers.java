import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;

/**
 * @author Palash Gandhi
 * @date 9/4/17. GraphDatabases
 */
public class DatabaseHelpers {

  public ArrayList<Node> get_all_nodes(GraphDatabaseService db) {
    ArrayList<Node> nodes = new ArrayList<>();
    for (Node n : db.getAllNodes()) {
      if (check_node_not_part_of_index(n, db)) {
        nodes.add(n);
      }
    }
    return nodes;
  }

  private boolean check_node_not_part_of_index(Node n, GraphDatabaseService db) {
    ArrayList<String> labels = get_labels(n);
    for (String label : labels) {
      if (label.contains("ignore_")) {
        return false;
      }
    }
    return true;
  }

  public ArrayList<Node> get_all_nodes_with_degree_greater_than_or_equal(int degree,
      GraphDatabaseService db) {
    ArrayList<Node> nodes = new ArrayList<>();
    for (Node n : db.getAllNodes()) {
      if (n.getDegree() >= degree) {
        nodes.add(n);
      }
    }
    return nodes;
  }


  public ArrayList<Node> get_neighbors(Node vertex, GraphDatabaseService db, Direction dir) {
    ArrayList<Node> neighbors = new ArrayList<>();
    Transaction tx = db.beginTx();
    try {
      for (Relationship rel : vertex.getRelationships(dir)) {
        Node neighbor = rel.getOtherNode(vertex);
        if (!neighbors.contains(neighbor)) {
          neighbors.add(neighbor);
        }
      }
    } catch (Exception e) {
      System.out.println("Exception occurred: " + e);
    }
    tx.close();
    return neighbors;
  }

  public ArrayList<String> get_labels(Node vertex) {
    ArrayList<String> labels = new ArrayList<>();
    for (Label l : vertex.getLabels()) {
      labels.add(l.name());
    }
    return labels;
  }

  public ArrayList<Node> find_possible_matches(Node vertex, GraphDatabaseService target_db) {
    ArrayList<Node> matches = new ArrayList<>();
    Transaction target_tx = target_db.beginTx();
    for (Node target_node : target_db.getAllNodes()) {
      if (get_labels(target_node).containsAll(get_labels(vertex))) {
        matches.add(target_node);
      }
    }
    target_tx.close();
    return matches;
  }

  // Helpers for SQBC

  public boolean is_vertex_in_clique(Node node, ArrayList<Node> clique) {
    for (Node clique_node : clique) {
      boolean connected = false;
      for (Relationship rel : node.getRelationships()) {
        if (rel.getOtherNode(node).equals(clique_node)) {
          connected = true;
        }
      }
      if (!connected) {
        return false;
      }
    }
    return true;
  }

  public ArrayList get_all_maximal_cliques(GraphDatabaseService db) {
    ArrayList cliques = new ArrayList();
    ArrayList current_clique = new ArrayList();
    ArrayList vertext_set = new ArrayList();
    for (Node node : db.getAllNodes()) {
      vertext_set.add(node);
    }
    ArrayList buffer_x = new ArrayList();
    bron_kerbosch(db, cliques, current_clique, vertext_set, buffer_x);
    return cliques;
  }

  private void bron_kerbosch(GraphDatabaseService db, ArrayList cliques, ArrayList current_clique,
      ArrayList<Node> vertex_set, ArrayList buffer) {
    if (vertex_set.size() == 0 && buffer.size() == 0 && current_clique.size() > 2) {
      cliques.add(current_clique);
    }
    ArrayList<Node> vertex_set_copy = new ArrayList<>(vertex_set);
    for (Node node : vertex_set) {
      ArrayList<Node> current_clique_union_node = new ArrayList<>(current_clique);
      current_clique_union_node.add(node);
      ArrayList<Node> vertex_set_intersection_node_neighbors = new ArrayList<>(vertex_set_copy);
      vertex_set_intersection_node_neighbors.retainAll(get_neighbors(node, db, Direction.BOTH));
      ArrayList<Node> buffer_intersection_node_neighbors = new ArrayList<>(buffer);
      buffer_intersection_node_neighbors.retainAll(get_neighbors(node, db, Direction.BOTH));
      bron_kerbosch(db, cliques, current_clique_union_node, vertex_set_intersection_node_neighbors,
          buffer_intersection_node_neighbors);
      vertex_set_copy.remove(node);
      buffer.add(node);
    }
  }

  public ArrayList<Node> get_clique_containing_node(ArrayList<ArrayList<Node>> cliques, Node node) {
    for (ArrayList<Node> clique : cliques) {
      if (clique.contains(node)) {
        return clique;
      }
    }
    return new ArrayList<>();
  }

  public Node get_node_with_highest_degree(GraphDatabaseService db) {
    int highest_degree = 0;
    Node highest_degree_node = null;
    for (Node node : db.getAllNodes()) {
      int degree = node.getDegree();
      if (degree > highest_degree) {
        highest_degree = degree;
        highest_degree_node = node;
      }
    }
    return highest_degree_node;
  }

  public ArrayList<Node> get_largest_max_clique_from_cliques(ArrayList<ArrayList<Node>> cliques) {
    ArrayList<Node> largest_clique = new ArrayList<>();
    for (ArrayList<Node> clique : cliques) {
      if (clique.size() > largest_clique.size()) {
        largest_clique = clique;
      }
    }
    return largest_clique;
  }

  public void compare_solution_and_ground_truth(String ground_truth_file_path,
      HashMap<Node, Node> solution, String query_file_name, String target_file_name) {
    File ground_truth_file = new File(ground_truth_file_path);
    FileInputStream fstream = null;
    BufferedReader br = null;
    try {
      fstream = new FileInputStream(ground_truth_file);
      br = new BufferedReader(new InputStreamReader(fstream));
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    String line;
    try {
      while ((line = br.readLine()) != null) {
        if (line.equals("T:" + target_file_name) && br.readLine().equals("P:" + query_file_name)) {
          int number_of_solutions = Integer.parseInt(br.readLine().split(":")[1]);
          for (int solution_num = 0; solution_num < number_of_solutions; solution_num++) {
            String solution_line = br.readLine();
            String solution_split = solution_line.split(":")[2];
            String[] ind = solution_split.split(";");
            boolean match = true;
            for (String val : ind) {
              String[] mapping = val.split(",");
              int query_node_id = Integer.parseInt(mapping[0]);
              int target_node_id = Integer.parseInt(mapping[1]);
              for (Node query_node : solution.keySet()) {
                if (query_node.getId() != query_node_id) {
                  continue;
                }
                if (target_node_id != solution.get(query_node).getId()) {
                  match = false;
                }
              }
            }
            if (match) {
//              System.out.println("Solution found in ground truth file. Line: " + solution_line);
              return;
            }
          }
//          System.out.println("Solution not found in ground truth file." + solution);
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public boolean is_super_vertex(VertexCode query_vertex_code, VertexCode target_vertex_code) {
    if (!target_vertex_code.labels.containsAll(query_vertex_code.labels)) {
      return false;
    }
    if (query_vertex_code.largest_maximal_clique_containing_vertex_size
        > target_vertex_code.largest_maximal_clique_containing_vertex_size) {
      return false;
    }
    for (String query_key : query_vertex_code.neighborhood_degree_info_labels.keySet()) {
      boolean found = false;
      for (String target_key : target_vertex_code.neighborhood_degree_info_labels.keySet()) {
        if (target_key.equals(query_key)) {
          if (is_presequence(query_vertex_code.neighborhood_degree_info_labels.get(query_key),
              target_vertex_code.neighborhood_degree_info_labels.get(target_key))) {
            found = true;
          }
        }
      }
      if (!found) {
        return false;
      }
    }
    return true;
  }

  public boolean is_presequence(ArrayList<Integer> list1, ArrayList<Integer> list2) {
    Collections.sort(list1, Collections.reverseOrder());
    Collections.sort(list2, Collections.reverseOrder());
    if (list1.size() > list2.size()) {
      return false;
    }
    for (Integer val1 : list1) {
      if (get_elements_greater_than_from_list(val1, list2) <= list1.indexOf(val1)) {
        return false;
      }
    }
    return true;
  }

  private int get_elements_greater_than_from_list(int val1, ArrayList<Integer> list2) {
    int count = 0;
    for (int val : list2) {
      if (val >= val1) {
        count += 1;
      }
    }
    return count;
  }

  // The main function that prints all combinations of size r
  // in arr[] of size n. This function mainly uses combinationUtil()
  static ArrayList<ArrayList<Node>> get_combinations(Node[] arr, int n, int r) {
    // A temporary array to store all combination one by one
    Node data[] = new Node[r];
    ArrayList<ArrayList<Node>> combinations = new ArrayList<>();

    // Print all combination using temprary array 'data[]'
    combinationUtil(arr, n, r, 0, data, 0, combinations);
    return combinations;
  }

  /* arr[]  ---> Input Array
    data[] ---> Temporary array to store current combination
    start & end ---> Staring and Ending indexes in arr[]
    index  ---> Current index in data[]
    r ---> Size of a combination to be printed */
  static void combinationUtil(Node arr[], int n, int r, int index, Node data[], int i,
      ArrayList<ArrayList<Node>> combinations) {
    // Current combination is ready to be printed, print it
    if (index == r) {
      combinations.add(new ArrayList(Arrays.asList(data)));
      return;
    }

    // When no more elements are there to put in data[]
    if (i >= n) {
      return;
    }

    // current is included, put next at next location
    data[index] = arr[i];
    combinationUtil(arr, n, r, index + 1, data, i + 1, combinations);

    // current is excluded, replace it with next (Note that
    // i+1 is passed, but index is not changed)
    combinationUtil(arr, n, r, index, data, i + 1, combinations);
  }

  public ArrayList<Node> get_one_step_reachable(Node v, GraphDatabaseService db) {
    return get_neighbors(v, db, Direction.BOTH);
  }

  public Set<Node> get_two_step_reachable(Node v, GraphDatabaseService database) {
    Set<Node> results = new HashSet();
    for (Node neighbor : get_neighbors(v, database, Direction.BOTH)) {
      results.addAll(get_one_step_reachable(neighbor, database));
    }
    return results;
  }
}
