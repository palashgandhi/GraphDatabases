import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import org.apache.commons.io.FileUtils;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;
import org.neo4j.unsafe.batchinsert.BatchInserter;
import org.neo4j.unsafe.batchinsert.BatchInserters;

/**
 * @author Palash Gandhi
 * @date 8/11/17. GraphDatabases
 */
public class FileDatabaseService {

  public String neo4j_database_folder_path;

  private BatchInserter bi;

  FileDatabaseService(String database_folder_path) {
    neo4j_database_folder_path = database_folder_path;
  }

  public GraphDatabaseService create_db(String file_path) {
    /**
     * Creates the query file and loads it into memory to make
     * computations faster. Also, this database is deleted at the end of
     * the execution
     * */
    System.out.println("For file " + file_path);
    File db_file = null;
    if (file_path.toLowerCase().contains("proteins")) {
      db_file = extract_data_from_proteins_file(file_path);
    } else if (file_path.toLowerCase().contains("igraph")) {
      db_file = extract_data_igraph(file_path);
    }
    GraphDatabaseService db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(db_file).
        setConfig(GraphDatabaseSettings.pagecache_memory, "512M").
        setConfig(GraphDatabaseSettings.string_block_size, "60").
        setConfig(GraphDatabaseSettings.array_block_size, "300").
        newGraphDatabase();
    return db;
  }

  private File extract_data_from_proteins_file(String file_path) {
    /**
     * Given the file, processes the file and creates databases.
     * @param file: The File object of the target graph.
     * */
    File file = new File(file_path);
    File db = new File(neo4j_database_folder_path + "/proteins." + file.getName());
    if (!db.exists()) {
      System.out.println("Creating database from " + file.getAbsolutePath() + "...");
      try {
        bi = BatchInserters.inserter(db);
      } catch (IOException e) {
        e.printStackTrace();
      }
      process_file(file.getAbsolutePath());
      bi.shutdown();
    }
    return db;
  }

  private void process_file(String file_path) {
    /**
     * Reads the file and creates nodes and relationships based on each
     * line of the file.
     **/
    FileInputStream file_inp_stream = null;
    try {
      file_inp_stream = new FileInputStream(file_path);
      BufferedReader br = new BufferedReader(new InputStreamReader(file_inp_stream));
      String line;
      int num_of_nodes = Integer.parseInt(br.readLine());
      while ((line = br.readLine()) != null) {
        if (line.contains("#") || line.split(" ").length == 0) {
          continue;
        }
        if (num_of_nodes > 0) {
          long node_id = Integer.parseInt(line.split(" ")[0]);
          String attribute = line.split(" ")[1];
          bi.createNode(node_id, new HashMap(), Label.label(attribute));
          num_of_nodes -= 1;
        } else {
          int num_of_edges = Integer.parseInt(line);
          for (int count_edge = 0; count_edge < num_of_edges; count_edge++) {
            String[] edge_line = br.readLine().split(" ");
            long from = Integer.parseInt(edge_line[0]);
            long to = Integer.parseInt(edge_line[1]);
            bi.createRelationship(from, to, RelationshipType.withName(""), new HashMap());
          }
        }
      }
      br.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void delete_temporary_db(String file_path) {
    try {
      System.out.println("\nDeleting temporary databases and file from " + file_path + "...");
      FileUtils.forceDelete(new File(file_path));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  File extract_data_igraph(String file_path) {
    /**
     * Given the file path, starts reading the graph file for igraphs and
     * creates the database, nodes and relationships based on what is read.
     * @param file_path: The path to the igraph file.
     * */
    File db = null;
    File file = new File(file_path);
    FileInputStream file_inp_stream = null;
    String line = null;
    try {
      file_inp_stream = new FileInputStream(file);
      BufferedReader br = new BufferedReader(new InputStreamReader(file_inp_stream));
      while ((line = br.readLine()) != null) {
        String[] line_arr = line.split(" ");
        if (line_arr[0].equals("t")) {
          db = new File(
              neo4j_database_folder_path + "/igraph." + line_arr[2] + "." + file.getName());
          if (db.exists()) {
            System.out.println("Database exists. Reusing...");
            return db;
          }
          System.out.println("Creating new graph database with name: " + db.getName());
          bi = BatchInserters.inserter(db);
        } else if (line_arr[0].equals("v")) {
          create_new_node_igraph(line_arr);
        } else if (line_arr[0].equals("e")) {
          create_relationship_igraph(line_arr);
        }
      }
      br.close();
      bi.shutdown();
      file_inp_stream.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
      System.exit(2);
    }
    System.out.println("Created new graph database with name: " + db.getName());
    return db;
  }

  File extract_data_igraph(String file_path, boolean is_query, int graph_num) {
    /**
     * Given the file path, starts reading the graph file for igraphs and
     * creates the database, nodes and relationships based on what is read.
     * @param file_path: The path to the igraph file.
     * @param is_query: True if task is to create a temporary query
     *                   database. Used only for naive subgraph matching
     *                   to create a query database.
     * @param graph_num: If specified, only create the specified graph
     *                       number from the query file. For e.g: to
     *                       create t # 10 from the query file,
     *                       graph_num = 10
     * */
    HashMap<Integer, Long> nodes = new HashMap<>();
    FileInputStream file_inp_stream = null;
    File file = new File(file_path);
    String line = null;
    File db = new File(neo4j_database_folder_path + "/igraph." + file.getName());
    try {
      bi = BatchInserters.inserter(db);
      file_inp_stream = new FileInputStream(file);
      BufferedReader br = new BufferedReader(new InputStreamReader(file_inp_stream));
      boolean already_created = false;
      while ((line = br.readLine()) != null) {
        String[] line_arr = line.split(" ");
        if (line_arr[0].equals("t")) {
          if (already_created) {
            break;
          }
          if (is_query && line_arr[2].equals(graph_num + "")) {
            System.out.println("Creating graph num: " + graph_num);
            already_created = true;
          }
        } else if (line_arr[0].equals("v") && already_created) {
          create_new_node_igraph(line_arr);
        } else if (line_arr[0].equals("e") && already_created) {
          create_relationship_igraph(line_arr);
        }
      }
      br.close();
      bi.shutdown();
      file_inp_stream.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
      System.exit(2);
    }
    return db;
  }


  void create_new_node_igraph(String[] line_arr) {
    /**
     * Creates a new node for igraph.
     * @param line_arr: Given the line from the file, create a node of
     *                the read id and labels.
     * */
    Label[] labels_list = new Label[line_arr.length - 2];
    long node_id = Integer.parseInt(line_arr[1]);
    for (int count = 2; count < line_arr.length; count++) {
      labels_list[count - 2] = Label.label(line_arr[count]);
    }
    bi.createNode(node_id, new HashMap(), labels_list);
  }

  void create_relationship_igraph(String[] line_arr) {
    /**
     * Creates a new relationship for igraph.
     * @param line_arr: Given the line from the file, create a
     *                relationship between the 2 nodes from the line_arr.
     * */
    long from_node_id = Integer.parseInt(line_arr[1]);
    long to_node_id = Integer.parseInt(line_arr[2]);
    String edge_label = line_arr[3];
    bi.createRelationship(from_node_id, to_node_id, RelationshipType.withName(edge_label),
        new HashMap());
  }
}
