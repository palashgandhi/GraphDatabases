/**
 * Class that represents a NEC tree.
 *
 * @author Palash Gandhi
 * @date 12/13/17. GraphDatabases
 */

import java.util.ArrayList;
import java.util.HashMap;


class NECTree {

  HashMap<NodeNEC, ArrayList<NodeNEC>> nec_tree_nodes;
  HashMap<NodeNEC, Integer> non_tree_edges;

  /**
   * @param nec_nodes List of NEC nodes in the NEC tree.
   * @param non_tree_edges_count Counts of the number of non-tree edges for each NEC node.
   */
  NECTree(HashMap<NodeNEC, ArrayList<NodeNEC>> nec_nodes,
      HashMap<NodeNEC, Integer> non_tree_edges_count) {
    nec_tree_nodes = nec_nodes;
    non_tree_edges = non_tree_edges_count;
  }
}
