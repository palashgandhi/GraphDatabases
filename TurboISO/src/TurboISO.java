/**
 * This class implements the TurboISO algorithm for subgraph isomorphism. It takes a neo4j query
 * database and a neo4j target database in the constructor and finds all solutions.
 *
 * @author Palash Gandhi
 */

import com.google.common.collect.Collections2;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;


public class TurboISO {

  private GraphDatabaseService query_db;
  private GraphDatabaseService target_db;

  private HashMap<NodeNEC, HashMap<Node, ArrayList<Node>>> candidate_region = new HashMap<>();
  private ArrayList<Node> visited = new ArrayList<>();

  private DatabaseHelpers helpers;
  private NodeNEC tree_root;

  private String query_file_path;
  private String target_file_path;
  private String ground_truth_file_path;

  public AnalysisArchive archive;

  /**
   * Constructor for class that takes and sets the query and target databases.
   *
   * @param query_database org.neo4j.graphdb.GraphDatabaseService object for query database.
   * @param target_database org.neo4j.graphdb.GraphDatabaseService object for target database.
   */
  TurboISO(GraphDatabaseService query_database, GraphDatabaseService target_database) {
    query_db = query_database;
    target_db = target_database;
    helpers = new DatabaseHelpers();
    archive = new AnalysisArchive();
  }

  /**
   * Main routine to be called that makes use of all TurboISO methods to find solutions.
   *
   * @param query_file_path String object that represents the query file in txt format from which
   * the database is built.
   * @param target_file_path String object that represents the target file in txt format from which
   * the database is built.
   * @param ground_truth_file_path String object that represents the graound truth file in txt
   * format that is used to verify the results.
   */
  public AnalysisArchive match(String query_file_path, String target_file_path,
      String ground_truth_file_path) {
    this.query_file_path = query_file_path;
    this.target_file_path = target_file_path;
    this.ground_truth_file_path = ground_truth_file_path;
    // CHOOSESTARTQVERTEX
    Node start_node = choose_start_query_vertex(query_db, target_db);
    System.out.println("Selected start node: " + start_node + "\n");
    // REWRTIETONECTREE
    NECTree nec_tree = rewrite_to_nec_tree(query_db, start_node);
    System.out.println("NEC tree: ");
    for (NodeNEC nec_node : nec_tree.nec_tree_nodes.keySet()) {
      System.out.println(nec_node);
    }
    System.out.println("\n");

    Transaction tx_target = target_db.beginTx();
    Transaction tx_query = query_db.beginTx();

    Node v_artificial = target_db.createNode(Label.label("TURBO_ISO_ARTIFICIAL"));

    long start_time = System.currentTimeMillis();
    ArrayList<Long> search_space_times = new ArrayList<>();

    for (Node target_node_vs : helpers.find_possible_matches(start_node, target_db)) {
      ArrayList<Node> candidate_data_vertex_set = new ArrayList<>();
      candidate_data_vertex_set.add(target_node_vs);
      long search_space_time = System.currentTimeMillis();
      if (!explore_cr(tree_root, candidate_data_vertex_set, v_artificial)
          || candidate_region.keySet().size() != nec_tree.nec_tree_nodes.size()) {
        search_space_times.add(search_space_time);
        continue;
      }
      System.out.println("Candidate region: " + candidate_region);
      search_space_times.add(search_space_time);
      ArrayList<NodeNEC> order = determine_matching_order(nec_tree);
      System.out.println("Matching order: " + order);

      ArrayList<Node> list_m_query = new ArrayList<>();
      ArrayList<Node> list_f_target = new ArrayList<>();
      update_state(list_m_query, list_f_target, start_node, target_node_vs);
      subgraph_search(query_db, nec_tree, target_db, order, 1, list_m_query, list_f_target);
      restore_state(list_m_query, list_f_target, start_node, target_node_vs);
    }

    tx_target.close();
    tx_query.close();
    long avg = 0;
    for (Long time : search_space_times) {
      avg += time;
    }
    archive.search_space_time = (avg / search_space_times.size());
    archive.solution_time = System.currentTimeMillis() - start_time;
    return archive;
  }

  /**
   * Selects the start query vertex by computing the ranks and selects the node with the lowest
   * rank.
   *
   * @param query_db org.neo4j.graphdb.GraphDatabaseService object for query database.
   * @param target_db org.neo4j.graphdb.GraphDatabaseService object for target database.
   */
  public Node choose_start_query_vertex(GraphDatabaseService query_db,
      GraphDatabaseService target_db) {
    Transaction tx_query = query_db.beginTx();
    double lowest_rank = -1;
    Node start_node = null;
    for (Node query_node : query_db.getAllNodes()) {
      int query_node_degree = query_node.getDegree();
      ArrayList<Node> possible_matches = helpers.find_possible_matches(query_node, target_db);
      double rank = (double) possible_matches.size() / query_node_degree;
      if (lowest_rank == -1 || rank < lowest_rank) {
        lowest_rank = rank;
        start_node = query_node;
      }
    }
    tx_query.close();
    return start_node;
  }

  /**
   * Rewrites the query database to a NEC tree.
   *
   * @param query_db org.neo4j.graphdb.GraphDatabaseService object for query database.
   * @param start_node The selected start node.
   */
  NECTree rewrite_to_nec_tree(GraphDatabaseService query_db, Node start_node) {
    HashMap<NodeNEC, ArrayList<NodeNEC>> nec_tree = new HashMap<>();
    HashMap<NodeNEC, Integer> non_tree_edges = new HashMap<>();
    // Create a root NEC vertex Us'.
    // Us'.NEC = [Us]
    ArrayList<Node> root_nec = new ArrayList<>();
    root_nec.add(start_node);
    NodeNEC root = new NodeNEC(root_nec);
    tree_root = root;

    ArrayList<Node> visited_nodes = new ArrayList<>();
    visited_nodes.add(start_node);

    ArrayList<NodeNEC> v_current = new ArrayList<>();
    ArrayList<NodeNEC> v_next = new ArrayList<>();
    v_next.add(root);

    do {
      v_current.addAll(v_next);
      v_next.clear();
      for (NodeNEC current_nec_vertex : v_current) {
        nec_tree.put(current_nec_vertex, new ArrayList<>());
        ArrayList<Node> adjacent_list = _get_adjacent_list(current_nec_vertex);
        //Remove all visited query vertices in C (adjacent_list)
        for (Node node : visited_nodes) {
          if (adjacent_list.contains(node)) {
            if (!non_tree_edges.containsKey(current_nec_vertex)) {
              non_tree_edges.put(current_nec_vertex, 0);
            }
            non_tree_edges.put(current_nec_vertex, non_tree_edges.get(current_nec_vertex) + 1);
            adjacent_list.remove(node);
          }
        }

        // Mark all visited query vertices in C (adjacent_list) as visited.
        for (Node node : adjacent_list) {
          visited_nodes.add(node);
        }
        if (!adjacent_list.isEmpty()) {
          Transaction tx_query = query_db.beginTx();
          HashMap<ArrayList<String>, ArrayList<Node>> groups_label = _group_by_label(adjacent_list);
          ArrayList<NodeNEC> nec_vertices;
          for (ArrayList<String> labels : groups_label.keySet()) {

            nec_vertices = find_nec(groups_label.get(labels));

            for (NodeNEC nec : nec_vertices) {
              if (!v_next.contains(nec)) {
                v_next.add(nec);
                nec.parent = current_nec_vertex;
                current_nec_vertex.children.add(nec);
              }
            }
            // Mark Uc' as parent of nec_vertices. How??
            ArrayList<NodeNEC> existing_vertices = nec_tree.get(current_nec_vertex);
            for (NodeNEC nec : nec_vertices) {
              existing_vertices.add(nec);
            }
            nec_tree.put(current_nec_vertex, existing_vertices);

          }
          tx_query.close();
        }
      }
      v_current.clear();
    } while (!v_next.isEmpty());
    NECTree nec_tree_archive = new NECTree(nec_tree, non_tree_edges);
    return nec_tree_archive;
  }

  /**
   * Builds the NEC nodes for a given list of nodes.
   *
   * @param nodes List of nodes from which the NEC nodes are built.
   * @return NECTree object.
   */
  private ArrayList<NodeNEC> find_nec(ArrayList<Node> nodes) {
    ArrayList<NodeNEC> nec_vertices = new ArrayList<>();
    for (Node node : nodes) {
      boolean node_added = false;
      for (NodeNEC nec : nec_vertices) {
        if (check_if_node_belongs_to_nec(node, nec)) {
          nec.NEC.add(node);
          node_added = true;
        }
      }
      if (!node_added) {
        ArrayList<Node> temp = new ArrayList<>();
        temp.add(node);
        nec_vertices.add(new NodeNEC(temp));

      }
    }
    return nec_vertices;
  }

  /**
   * Performs the checks required to add a node to a NEC node.
   *
   * @param node Node object that has to be checked.
   * @param nec NEC node to which the node node can/cannot be added.
   * @return boolean
   */
  boolean check_if_node_belongs_to_nec(Node node, NodeNEC nec) {
    Node node_nec = nec.NEC.get(0);
    Iterable<Label> label_nec = node_nec.getLabels();
    Iterable<Label> label = node.getLabels();
    if (label.equals(label_nec)) {
      Iterable<Relationship> relationships_node = node.getRelationships();
      ArrayList<Node> connected_to_node = new ArrayList<>();
      for (Relationship rel : relationships_node) {
        connected_to_node.add(rel.getOtherNode(node));
      }
      ArrayList<Node> connected_to_each_nec_node = new ArrayList<>();
      for (Node nec_node : nec.NEC) {
        Iterable<Relationship> relationships_nec_node = nec_node.getRelationships();
        for (Relationship rel : relationships_nec_node) {
          connected_to_each_nec_node.add(rel.getOtherNode(nec_node));
        }
      }
      if (connected_to_each_nec_node.size() != connected_to_node.size()) {
        return false;
      } else {
        for (Node node_connection : connected_to_node) {
          if (!connected_to_each_nec_node.contains(node_connection)) {
            return false;
          }
        }
      }
    }
    return true;
  }

  /**
   * Given a list of nodes, groups them by labels
   *
   * @param adjacent_list List of nodes.
   * @return A HashMap of the labels as the key and the grouped nodes as the values.
   */
  private HashMap<ArrayList<String>, ArrayList<Node>> _group_by_label(
      ArrayList<Node> adjacent_list) {
    HashMap<ArrayList<String>, ArrayList<Node>> group = new HashMap<>();
    for (Node node : adjacent_list) {
      ArrayList<String> labels = helpers.get_labels(node);
      if (!group.containsKey(labels)) {
        group.put(labels, new ArrayList<>());
      }
      ArrayList<Node> nodes = group.get(labels);
      nodes.add(node);
      group.put(labels, nodes);
    }
    return group;
  }

  /**
   * Given a NEC vertex, returns all the neighbors of the nodes in the NEC.
   *
   * @param nec_vertex Type NodeNEC.
   * @return List of neighbors of the nodes in the NEC node.
   */
  private ArrayList<Node> _get_adjacent_list(NodeNEC nec_vertex) {
    ArrayList<Node> adjacent_list = new ArrayList<>();
    for (Node node : nec_vertex.NEC) {
      for (Node temp_node : helpers.get_neighbors(node, query_db, Direction.BOTH)) {
        if (!adjacent_list.contains(temp_node)) {
          adjacent_list.add(temp_node);
        }
      }
    }
    return adjacent_list;
  }

  /**
   * EXPLORCR (explore candidate region) method from TurboISO.
   *
   * @param u_prime NEC node for which to explore the candidate region.
   * @param target_node_vs List of nodes that are possible matches to the NEC node.
   * @param v Parent of candidate for u.
   * @return boolean
   */
  boolean explore_cr(NodeNEC u_prime, ArrayList<Node> target_node_vs, Node v) {
    for (Node v_prime : target_node_vs) {
      if (visited.contains(v_prime)) {
        continue;
      }
      if (u_prime.NEC.get(0).getDegree() > v_prime.getDegree()
          || !neigborhood_label_frequency_filter(u_prime.NEC.get(0), v_prime)) {
        continue;
      }

      visited.add(v_prime);

      boolean matched = true;

      ArrayList<NodeNEC> u_children = u_prime.children;
      Collections.sort(u_children, Comparator
          .comparing(u -> new Integer(get_adj(v_prime, helpers.get_labels(u.NEC.get(0))).size())));
      ArrayList<NodeNEC> visited_nec_nodes = new ArrayList<>();
      for (NodeNEC child : u_children) {
        if (!explore_cr(child, get_adj(v_prime, helpers.get_labels(child.NEC.get(0))), v_prime)) {
          for (NodeNEC visited_nec : visited_nec_nodes) {
            if (visited_nec.equals(child)) {
              continue;
            }
            clear_cr(visited_nec, v_prime, candidate_region);
            matched = false;
            break;
          }
          visited_nec_nodes.add(child);
        }
      }

      visited.remove(v_prime);

      if (!matched) {
        continue;
      }

      //append v_prime into CR(u_prime, v)
      if (!candidate_region.containsKey(u_prime)) {
        candidate_region.put(u_prime, new HashMap<Node, ArrayList<Node>>());
      }
      HashMap<Node, ArrayList<Node>> existing_hmap_val_u_prime = candidate_region.get(u_prime);
      if (!existing_hmap_val_u_prime.containsKey(v)) {
        existing_hmap_val_u_prime.put(v, new ArrayList<Node>());
      }
      ArrayList<Node> existing_nodes = existing_hmap_val_u_prime.get(v);
      if (!existing_nodes.contains(v_prime)) {
        existing_nodes.add(v_prime);
      }
      existing_hmap_val_u_prime.put(v, existing_nodes);
      candidate_region.put(u_prime, existing_hmap_val_u_prime);
    }

    if (candidate_region.containsKey(u_prime) && candidate_region.get(u_prime).containsKey(v)
        && candidate_region.get(u_prime).get(v).size() < u_prime.NEC.size()) {
      clear_cr(u_prime, v, candidate_region);
      return false;
    }
    return true;

  }

  /**
   * CLEARCR (clear candidate region from TurboISO)
   *
   * @param u NEC node for which the CR needs to be cleared.
   * @param v Parent of candidate for u.
   * @param cr Current candidate region.
   */
  private void clear_cr(NodeNEC u, Node v, HashMap<NodeNEC, HashMap<Node, ArrayList<Node>>> cr) {
    cr.get(u).remove(v);
  }

  /**
   * NLF filter that checks if |adj(u, l)| <= |adj(v, l)|
   *
   * @param node Current query node.
   * @param data_vertex_v Current data node.
   * @return boolean
   */
  private boolean neigborhood_label_frequency_filter(Node node, Node data_vertex_v) {
    return get_adj(node, helpers.get_labels(node)).size() <= get_adj(data_vertex_v,
        helpers.get_labels(node)).size();
  }

  /**
   * Given a list of labels, returns all the neighbors that contain all of those labels.
   *
   * @param node Current node in consideration.
   * @param label List of labels.
   * @return List of neighbors that satisfy the condition.
   */
  private ArrayList<Node> get_adj(Node node, ArrayList<String> label) {
    ArrayList<Node> adj = new ArrayList<>();
    for (Relationship rel_u : node.getRelationships()) {
      Node neighbor = rel_u.getOtherNode(node);
      try {
        if (helpers.get_labels(neighbor).containsAll(label)) {
          adj.add(neighbor);
        }
      } catch (Exception e) {
        System.out.println("Exception occurred: " + e);
      }
    }
    return adj;
  }

  /**
   * Determines the matching order given a NEC tree.
   *
   * @param nec_tree NECTree object.
   * @return List of NEC nodes in the matching order.
   */
  private ArrayList<NodeNEC> determine_matching_order(NECTree nec_tree) {
    ArrayList<NodeNEC> order = new ArrayList<>();
    HashMap<NodeNEC, Integer> ranks = new HashMap<>();
    ArrayList<NodeNEC> unvisited = get_nec_list_in_order(tree_root);
    for (NodeNEC nec_node : unvisited) {
      order.add(nec_node);
      int cr_size = 0;
      if (nec_node.NEC.size() > 1) {
        for (ArrayList<Node> v_list : candidate_region.get(nec_node.parent).values()) {
          for (Node v : v_list) {
            if (candidate_region.get(nec_node).containsKey(v)) {
              cr_size += combine(candidate_region.get(nec_node).get(v).size(), nec_node.NEC.size());
            }
          }
        }
      } else {
        for (Node node : candidate_region.get(nec_node).keySet()) {
          cr_size += candidate_region.get(nec_node).get(node).size();
        }
      }
      int parent_rank = 0;
      for (NodeNEC nec : nec_tree.nec_tree_nodes.keySet()) {
        if (nec.children.contains(nec_node)) {
          parent_rank = ranks.get(nec);
        }
      }
      int j = 0;
      if (nec_tree.non_tree_edges.get(nec_node) != null) {
        j = nec_tree.non_tree_edges.get(nec_node);
      }
      ranks.put(nec_node, Math.max(parent_rank, cr_size / (j + 1)));
    }
    order.sort(Comparator.comparing(ranks::get));
    return order;
  }

  /**
   * Helper method for determining matching order that calculates nCr.
   *
   * @param n Type int
   * @param r Type int
   * @return Value of nCr.
   */
  private int combine(int n, int r) {
    return factorial(n) / (factorial(n - r) * factorial(r));
  }

  /**
   * Returns the factorial of a number. Helper method while determining matching order.
   *
   * @param num The number of which the factorial has to be calculated.
   * @return the factorial.
   */
  private int factorial(int num) {
    int fact = 1, i;
    for (i = 1; i <= num; i++) {
      fact = fact * i;
    }
    return fact;
  }

  /**
   * Flattens the NEC tree.
   *
   * @param node Flattens the NEC tree beginning from this node as the root.
   * @return List of NEC nodes in the subtree.
   */
  private ArrayList<NodeNEC> get_nec_list_in_order(NodeNEC node) {
    ArrayList<NodeNEC> list = new ArrayList<>();
    if (!list.contains(node)) {
      list.add(node);
    }
    if (node.children.size() == 0) {
      return list;
    }
    for (NodeNEC child : node.children) {
      list.addAll(get_nec_list_in_order(child));
    }
    return list;
  }

  /**
   * Main recursive subroutine that performs the backtracking.
   *
   * @param query_db GraphDatabaseService object representing the query database.
   * @param nec_tree NECTree object representing the NEC tree.
   * @param target_db GraphDatabaseService object representing the target database.
   * @param order Current determined matching order.
   * @param index Current index to be matched.
   * @param list_m_query List of query nodes in current mapping.
   * @param list_f_target List of corresponding matched target nodes in current mapping.
   */
  public void subgraph_search(GraphDatabaseService query_db, NECTree nec_tree,
      GraphDatabaseService target_db, ArrayList<NodeNEC> order, int index,
      ArrayList<Node> list_m_query, ArrayList<Node> list_f_target) {
    if (index >= order.size()) {
      return;
    }
    NodeNEC u_prime = order.get(index);
    ArrayList<Node> candidate_vertices = get_candidate_vertices(u_prime, list_m_query,
        list_f_target);
    ArrayList<ArrayList<Node>> combinations = generate_comb(u_prime, candidate_vertices);
    int count_combination = 0;
    while (combinations.size() > 0 && count_combination < combinations.size()) {

      ArrayList<Node> c_prime = combinations.get(count_combination++);

      if (c_prime.size() == 0) {
        return;
      }

      if (already_mapped(c_prime, list_f_target)) {
        continue;
      }

      if (forms_clique(u_prime.NEC, query_db) && !forms_clique(c_prime, target_db)) {
        continue;
      }

      boolean matched = true;

      for (int i = 0; i < u_prime.NEC.size(); i++) {
        if (!is_joinable(query_db, target_db, list_m_query, list_f_target, u_prime.NEC.get(i),
            c_prime.get(i))) {
          matched = false;
          break;
        }
      }

      if (!matched) {
        continue;
      }

      update_state(list_m_query, list_f_target, u_prime, c_prime);

      if (nec_tree.nec_tree_nodes.size() - 1 == index) {
        generate_permutations(list_m_query, list_f_target, nec_tree, 0);
      } else {
        subgraph_search(query_db, nec_tree, target_db, order, index + 1, list_m_query,
            list_f_target);
      }

      restore_state(list_m_query, list_f_target, u_prime, c_prime);
    }
  }

  /**
   * Checks if the current mapping includes any of the candidates in consideration.
   *
   * @param c_prime List of candidate nodes to be checked.
   * @param list_f_target List of matched data nodes in current mapping.
   * @return boolean
   */
  private boolean already_mapped(ArrayList<Node> c_prime, ArrayList<Node> list_f_target) {
    for (Node c : c_prime) {
      if (list_f_target.contains(c)) {
        return true;
      }
    }
    return false;
  }

  /**
   * ISJOINABLE or CAN_MAP or method that performs check if a given query node can be mapped to the
   * current data node in consideration.
   *
   * @param query_db GraphDatabaseService object representing the query database.
   * @param target_db GraphDatabaseService object representing the target database.
   * @param list_m_query List of matched query nodes in current mapping.
   * @param list_f_target List of matched data nodes in current mapping.
   * @param query_node Current query node in consideration.
   * @param target_node Current target node in consideration.
   * @return boolean
   */
  private boolean is_joinable(GraphDatabaseService query_db, GraphDatabaseService target_db,
      ArrayList<Node> list_m_query, ArrayList<Node> list_f_target, Node query_node,
      Node target_node) {
    for (Node matched_query_node : list_m_query) {
      if (matched_query_node.getId() != query_node.getId() && helpers
          .get_neighbors(query_node, query_db, Direction.BOTH).contains(matched_query_node)) {
        Node matched_target_node = list_f_target.get(list_m_query.indexOf(matched_query_node));
        if (!helpers.get_neighbors(matched_target_node, target_db, Direction.BOTH)
            .contains(target_node)) {
          return false;
        }
      }
    }
    return true;
  }

  /**
   * GENPERM method from TurboISO. Each time a successful mapping of all query nodes to data nodes
   * is obtained, TurboISO proceeds to generate all permutations of the query vertices of each NEC
   * node in the NEC tree. If there is a single query node in a NEC node, the next node is
   * considered. If there are more than 1 query nodes within the current NEC node, then all possible
   * permutations are generated for these query nodes and multiple embeddings are found.
   *
   * @param list_m_query List of matched query nodes in current mapping.
   * @param list_f_target List of matched target nodes in current mapping.
   * @param nec_tree NECTree object representing the NEC tree.
   * @param i Current index.
   */
  private void generate_permutations(ArrayList<Node> list_m_query, ArrayList<Node> list_f_target,
      NECTree nec_tree, int i) {
    if (i == nec_tree.nec_tree_nodes.size()) {
      HashMap<Node, Node> solution = new HashMap<>();
      for (Node node : list_m_query) {
        solution.put(node, list_f_target.get(list_m_query.indexOf(node)));
      }
      System.out.print("[");
      for (Node query : solution.keySet()) {
        System.out.print(
            "u\\textsubscript{" + query.getId() + "}: v\\textsubscript{" + solution.get(query)
                .getId() + "}" + ", ");
      }
      System.out.println("]\n");
      archive.solutions.add(solution);
      if (ground_truth_file_path != null && ground_truth_file_path.length() > 0) {
        helpers.compare_solution_and_ground_truth(ground_truth_file_path, solution,
            new File(query_file_path).getName(), new File(target_file_path).getName());
      }
      return;
    }
    NodeNEC[] nec_nodes = new NodeNEC[nec_tree.nec_tree_nodes.size()];
    nec_tree.nec_tree_nodes.keySet().toArray(nec_nodes);
    if (nec_nodes[i].NEC.size() == 1) {
      generate_permutations(list_m_query, list_f_target, nec_tree, i + 1);
    } else {
      Collection<List<Node>> perms = permute(list_m_query, list_f_target, nec_nodes[i]);
      for (List<Node> perm : perms) {
        for (int count = 0; count < perm.size(); count++) {
          Node query = nec_nodes[i].NEC.get(count);
          Node tgt = perm.get(count);
          list_f_target.set(list_m_query.indexOf(query), tgt);
        }
        generate_permutations(list_m_query, list_f_target, nec_tree, i + 1);
      }
    }
  }

  /**
   * Given a list of target nodes, generates all permutations.
   *
   * @param list_m_query List of matched query nodes in current mapping.
   * @param list_f_target List of matched target nodes in current mapping.
   * @param nec_node Permutations of the matches of this NEC node are returned.
   * @return list of lists of all permutations.
   */
  private Collection<List<Node>> permute(ArrayList<Node> list_m_query,
      ArrayList<Node> list_f_target, NodeNEC nec_node) {
    ArrayList<Node> target = new ArrayList();
    for (Node node : nec_node.NEC) {
      target.add(list_f_target.get(list_m_query.indexOf(node)));
    }
    Collection<List<Node>> orderPerm = Collections2.permutations(target);
    return orderPerm;
  }

  /**
   * Adds the query_node to list_m_query and sets target_node in list_f_target at the same index of
   * query_node in list_m_query.
   *
   * @param list_m_query List of query vertices in current mapping.
   * @param list_f_target Corresponding list of matched data nodes in current mapping.
   * @param query_node The query node to be mapped.
   * @param target_node The target node to be mapped.
   */
  private void update_state(ArrayList<Node> list_m_query, ArrayList<Node> list_f_target,
      Node query_node, Node target_node) {
    if (!list_m_query.contains(query_node)) {
      list_m_query.add(query_node);
    }
    if (list_f_target.size() <= list_m_query.indexOf(query_node)) {
      list_f_target.add(list_m_query.indexOf(query_node), target_node);
    } else {
      list_f_target.set(list_m_query.indexOf(query_node), target_node);
    }
  }

  /**
   * Removes the query_node to list_m_query and sets target_node in list_f_target at the same index
   * of query_node in list_m_query.
   *
   * @param list_m_query List of query vertices in current mapping.
   * @param list_f_target Corresponding list of matched data nodes in current mapping.
   * @param query_node The query node to be removed.
   * @param target_node The target node to be removed.
   */
  private void restore_state(ArrayList<Node> list_m_query, ArrayList<Node> list_f_target,
      Node query_node, Node target_node) {
    list_f_target.remove(target_node);
    list_m_query.remove(query_node);
  }

  /**
   * Overloaded method that maps multiple data nodes to all nodes in a NEC node.
   *
   * @param list_m_query List of query vertices in current mapping.
   * @param list_f_target Corresponding list of matched data nodes in current mapping.
   * @param u_prime NEC node in consideration.
   * @param c_prime List of candidates of the current NEC node.
   */
  private void update_state(ArrayList<Node> list_m_query, ArrayList<Node> list_f_target,
      NodeNEC u_prime, ArrayList<Node> c_prime) {
    for (int i = 0; i < u_prime.NEC.size(); i++) {
      Node query_node = u_prime.NEC.get(i);
      Node target_node = c_prime.get(i);
      if (!list_m_query.contains(query_node)) {
        list_m_query.add(query_node);
      }
      if (list_f_target.size() <= list_m_query.indexOf(query_node)) {
        list_f_target.add(list_m_query.indexOf(query_node), target_node);
      } else {
        list_f_target.set(list_m_query.indexOf(query_node), target_node);
      }
    }
  }

  /**
   * Overloaded method that removes multiple data nodes and all nodes in a NEC node.
   *
   * @param list_m_query List of query vertices in current mapping.
   * @param list_f_target Corresponding list of matched data nodes in current mapping.
   * @param u_prime NEC node in consideration.
   * @param c_prime List of candidates of the current NEC node.
   */
  private void restore_state(ArrayList<Node> list_m_query, ArrayList<Node> list_f_target,
      NodeNEC u_prime, ArrayList<Node> c_prime) {
    for (int i = 0; i < u_prime.NEC.size(); i++) {
      Node query_node = u_prime.NEC.get(i);
      Node target_node = c_prime.get(i);
      list_f_target.remove(target_node);
      list_m_query.remove(query_node);
    }
  }

  /**
   * Generates all combinations of the given list of candidates.
   *
   * @param u_prime Current NEC node in consideration.
   * @param candidate_vertices List of candidates for u_prime.
   * @return List of lists of all combinations.
   */
  private ArrayList<ArrayList<Node>> generate_comb(NodeNEC u_prime,
      ArrayList<Node> candidate_vertices) {
    Node[] candidates = new Node[candidate_vertices.size()];
    candidates = candidate_vertices.toArray(candidates);
    return DatabaseHelpers.get_combinations(candidates, candidates.length, u_prime.NEC.size());
  }

  /**
   * Checks if a given list of nodes form a clique.
   *
   * @param list List of nodes to be checked.
   * @param db GraphDatabaseService object that contains the nodes.
   * @return boolean
   */
  private boolean forms_clique(ArrayList<Node> list, GraphDatabaseService db) {
    for (Node node : list) {
      for (Node neighbor : list) {
        if (neighbor.getId() != node.getId() && !helpers.get_neighbors(node, db, Direction.BOTH)
            .contains(neighbor)) {
          return false;
        }
      }
    }
    return true;
  }

  /**
   * Generates a list of candidate given a NEC node.
   *
   * @param u_prime NEC node for which the candidates are to be generated.
   * @param list_m_query List of query nodes in current mapping.
   * @param list_f_target Corresponding list of target nodes in current mapping.
   * @return List of candidates.
   */
  private ArrayList<Node> get_candidate_vertices(NodeNEC u_prime, ArrayList<Node> list_m_query,
      ArrayList<Node> list_f_target) {
    ArrayList<Node> candidates = new ArrayList<>();
    ArrayList<Node> nec_list = u_prime.parent.NEC;
    for (Node parent_query_nec : nec_list) {
      Node match_of_nec = list_f_target.get(list_m_query.indexOf(parent_query_nec));
      if (candidate_region.get(u_prime).containsKey(match_of_nec)) {
        if (candidates.size() == 0) {
          candidates.addAll(candidate_region.get(u_prime).get(match_of_nec));
        } else {
          candidates.retainAll(candidate_region.get(u_prime).get(match_of_nec));
        }
      }
    }
    return candidates;
  }

  public static void main(String[] args) {

    String query_file_path = "";
    String target_file_path = "";
    String ground_truth_file_path = "";
    GraphDatabaseService query_db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(
        new File("/home/pbg4930/Desktop/neo4j-community-3.1.1/data/databases/proteins.89.9.txt")).
        setConfig(GraphDatabaseSettings.pagecache_memory, "512M").
        setConfig(GraphDatabaseSettings.string_block_size, "60").
        setConfig(GraphDatabaseSettings.array_block_size, "300").
        newGraphDatabase();
    GraphDatabaseService target_db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(
        new File("/home/pbg4930/Desktop/neo4j-community-3.1.1/data/databases/proteins.99.txt")).
        setConfig(GraphDatabaseSettings.pagecache_memory, "512M").
        setConfig(GraphDatabaseSettings.string_block_size, "60").
        setConfig(GraphDatabaseSettings.array_block_size, "300").
        newGraphDatabase();

//    query_file_path = "/home/pbg4930/Desktop/PaperExample/PaperExample/query/89.9.txt";
//    target_file_path = "/home/pbg4930/Desktop/PaperExample/PaperExample/target/99.txt";
//    ground_truth_file_path = "";
//    FileDatabaseService file_helper = new FileDatabaseService(
//        "/home/pbg4930/Desktop/neo4j-community-3.1.1/data/databases/");
//    query_db = file_helper.create_db(query_file_path);
//    target_db = file_helper.create_db(target_file_path);
    try {
      TurboISO matcher = new TurboISO(query_db, target_db);
      matcher.match(query_file_path, target_file_path, ground_truth_file_path);
    } catch (Exception e) {
      System.out.println(e);
      e.printStackTrace();
    }
  }
}
