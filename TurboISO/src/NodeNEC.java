/**
 * Class that represents a NEC node.
 *
 * @author Palash Gandhi
 * @date 12/13/17. GraphDatabases
 */

import java.util.ArrayList;
import org.neo4j.graphdb.Node;


class NodeNEC {

  ArrayList<NodeNEC> children;
  ArrayList<Node> NEC;
  NodeNEC parent;

  /**
   * @param NEC List of nodes in the NEC node.
   */
  NodeNEC(ArrayList<Node> NEC) {
    this.children = new ArrayList<>();
    this.NEC = NEC;
    this.parent = null;
  }

  public String toString() {
    return "NodeNEC [NEC: " + this.NEC.toString() + "]";
  }
}
