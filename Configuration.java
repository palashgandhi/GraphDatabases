/**
 * @author Palash Gandhi
 * @date 11/30/17. GraphDatabases
 */
public class Configuration {

  String query_file_path;
  String target_file_path;
  String ground_truth_file_path;

  Configuration() {
    query_file_path = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/query/backbones_1EMA.8.sub.grf";
    target_file_path = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/target/backbones_1O54.grf";
    ground_truth_file_path = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/ground_truth/Proteins.8.gtr";
  }
}
