import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.commons.io.FileUtils;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;
import org.neo4j.unsafe.batchinsert.BatchInserter;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 23/3/17 3:47 PM CSCI729_Graph_Databases Run Insturctions:
 */
public class NaiveSubgraphMatcher {

  /**
   *
   * */
  static String PROTEINS_PATH = "/home/palash/Desktop/graph_db/Proteins/Proteins";
  static String IGRAPH_PATH = "/home/palash/Desktop/graph_db/iGraph";
  String neo4j_database_folder_path = "/home/pbg4930/Desktop/neo4j-community-3.1.1/data/databases/";
  String query_file_path;
  String target_file_path;
  GraphDatabaseService query_db;
  GraphDatabaseService target_db;
  HashMap<Node, ArrayList<Node>> search_space;
  ArrayList<Node> search_order;
  HashMap<Node, Node> solution;
  Iterable<Node> query_nodes;
  int query_nodes_size;
  int query_size;
  int num_of_solutions_gt;
  int limit;
  File gt_file;
  BatchInserter bi;
  File query_file;
  File target_file;

  NaiveSubgraphMatcher(String target_file_path, String query_file_path, int limit) {
    /**
     * Constructor for class
     * @param target_db_path: The path to the database folder.
     * @param query_file_path: The path to the query file.
     * @param limit: The number of solutions to find.
     * */
    this.query_file_path = query_file_path;
    this.target_file_path = target_file_path;
    this.limit = limit;
    if (target_file_path.contains("proteins")) {
      query_size = Integer.parseInt(
          query_file_path.substring(query_file_path.indexOf(".") + 1, query_file_path.indexOf("" +

              ".sub" + ".grf")));
      gt_file = new File(PROTEINS_PATH + "/ground_truth/Proteins" + "." + query_size + ".gtr");
    }
    search_space = new HashMap<>();
    search_order = new ArrayList<>();
    solution = new HashMap<>();
    FileDatabaseService file_helper = new FileDatabaseService(neo4j_database_folder_path);
    query_file = new File(query_file_path);
    target_file = new File(target_file_path);
    query_db = file_helper.create_db(query_file_path);
    target_db = file_helper.create_db(target_file_path);
  }

  public static void main(String[] args) {
    String query_file_path = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/query/backbones_1EMA.8.sub.grf";
    String target_file_path = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/target/backbones_1EMA.grf";
    NaiveSubgraphMatcher matcher = new NaiveSubgraphMatcher(target_file_path, query_file_path, 1000);
    matcher.match();
  }

  void match() {
    /**
     * Function that computes the search space, searcch order and calls
     * the backktrack search function.
     * */
    compute_search_space();
    compute_search_order();
    System.out.println("\n\nSearching for subgraph...");
    System.out.println("\n==========================================================");
    backtrack_search(0, solution);
    System.out.println("==========================================================");
    query_db.shutdown();
    target_db.shutdown();
  }

  void compute_search_space() {
    /**
     * Computes the search space based on the labels of each query and
     * target nodes.
     * */
    System.out.println("Computing search space...");
    Transaction tx_query = query_db.beginTx();
    Transaction tx_target = target_db.beginTx();
    query_nodes = query_db.getAllNodes();
    query_nodes_size = 0;
    for (Node node : query_nodes) {
      query_nodes_size += 1;
    }
    for (Node node : query_nodes) {
      ArrayList<String> query_node_labels = get_label(node);
      ArrayList<Node> matched_target_nodes = new ArrayList<>();
      for (String query_node_label : query_node_labels) {
        ResourceIterator<Node> target_nodes = target_db.findNodes(Label.label(query_node_label));
        for (ResourceIterator<Node> it = target_nodes; it.hasNext(); ) {
          Node target_node = it.next();
          matched_target_nodes.add(target_node);
        }
      }
      search_space.put(node, matched_target_nodes);
    }
    for (Node node : search_space.keySet()) {
      System.out
          .println("For node: " + node + " search space size: " + search_space.get(node).size());
    }
    tx_query.close();
    tx_target.close();
  }

  void compute_search_order() {
    /**
     * Computes the search order in a DFS style.
     * */
    for (Node node : search_space.keySet()) {
      System.out
          .println("For node: " + node + " search space: " + search_space.get(node).size());
    }
    Transaction tx_query = query_db.beginTx();
    Transaction tx_target = target_db.beginTx();
    query_nodes = query_db.getAllNodes();
    System.out.println("Computing search order...");
    ArrayList<Node> failed_check_nodes = new ArrayList<>();
    Node start_node = get_node_with_smallest_search_space();
    search_order.add(start_node);
    for (Node query_node : search_space.keySet()) {
      boolean to_add = check_if_nodes_connected(query_node);
      if (to_add && !search_order.contains(query_node)) {
        search_order.add(query_node);
      } else {
        failed_check_nodes.add(query_node);
      }
    }
    for (Node query_node_failed_check : failed_check_nodes) {
      if (!search_order.contains(query_node_failed_check)) {
        search_order.add(query_node_failed_check);
      }
    }
    tx_query.close();
    tx_target.close();
  }

  private Node get_node_with_smallest_search_space() {
    /**
     * Returns the node with the smallest search space.
     * */
    int min_size_search_space = -1;
    Node start_node = null;
    for (Node query_node : search_space.keySet()) {
      if (min_size_search_space == -1
          || search_space.get(query_node).size() < min_size_search_space) {
        min_size_search_space = search_space.get(query_node).size();
        start_node = query_node;
      }
    }
    return start_node;
  }

  void backtrack_search(int i, HashMap<Node, Node> solution) {
    /**
     * Function that computes the solution by doing a backtrack search
     * recursively.
     * */
    Transaction tx_query = query_db.beginTx();
    Transaction tx_target = target_db.beginTx();
    if (limit <= 0) {
      return;
    }
    if (solution.size() == query_nodes_size) {
      limit -= 1;
      System.out.println("Computed solution: " + solution.toString());
      if (query_file_path.toLowerCase().contains("proteins")) {
        System.out.println("Checking ground truth....");
        DatabaseHelpers helper = new DatabaseHelpers();
        helper.compare_solution_and_ground_truth("/home/pbg4930/Desktop/graph_db/Proteins/Proteins/ground_truth/Proteins.8.gtr", solution, query_file.getName(), target_file.getName());
      }
    } else {
      Node u = search_order.get(i);
      for (Node v : search_space.get(u)) {
        if (!solution.values().contains(v)) {
          if (can_map(u, v, solution)) {
            solution.put(u, v);
            backtrack_search(i + 1, solution);
            solution.remove(u);
          }
        }
      }
    }
    tx_query.close();
    tx_target.close();
  }

  boolean can_map(Node u, Node v, HashMap<Node, Node> solution_param) {
    /**
     * Check function that checks an edge from the query and target
     * graphs.
     * */
    boolean res = true;
    for (Node query_node : query_nodes) {
      if (query_node.getId() == u.getId()) {
        continue;
      }
      if (solution_param.containsKey(query_node)) {
        for (Relationship rel : query_node.getRelationships()) {
          if (rel.getOtherNode(query_node).getId() == u.getId()) {
            boolean exists = false;
            for (Relationship rel2 : v.getRelationships()) {
              if (rel2.getOtherNode(v).getId() == solution_param.get(query_node).getId()) {
                exists = true;
                break;
              }
            }
            res = exists;
          }
        }
      }
    }
    return res;
  }

  private boolean check_if_nodes_connected(Node query_node1) {
    /**
     * Checks if a node is connected to any other node in the current
     * search order.
     * */
    for (Node query_node2 : search_order) {
      Iterable<Relationship> relationships = query_node1.getRelationships();
      for (Relationship relationship : relationships) {
        if (query_node2.getId() == relationship.getOtherNode(query_node1).getId()) {
          return true;
        }
      }
    }
    return false;
  }

  ArrayList<String> get_label(Node node) {
    /**
     * Helper function to fetch the labels from the database for a
     * particular node.
     * */
    ArrayList<String> label_names = new ArrayList<>();
    Iterable<Label> labels = node.getLabels();
    for (Label label : labels) {
      label_names.add(label.name());
    }
    return label_names;
  }
}
