import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.ResourceIterable;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;
import org.neo4j.unsafe.batchinsert.BatchInserter;
import org.neo4j.unsafe.batchinsert.BatchInserters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 24/3/17 7:56 PM
 * CSCI729_Graph_Databases
 */
public class Migrate {
    /**
     * Class that reads each target file from proteins/ and igraph/ and creates
     * the graph databases on the disk.
     * Run instructions: Please edit the following 3 variables and set them
     * according to yoour system:
     * 1. PROTEINS_PATH: The path to the proteins folder.
     * e.g: /home/palash/Desktop/graph_db/Proteins/Proteins
     * 2. IGRAPH_PATH: The path to the iGraph folder.
     * e.g: /home/palash/Desktop/graph_db/iGraph
     * 3. neo4j_database_folder_path: The path where you want your databases
     * created.
     * e.g: /home/palash/Desktop/neo4j-community-3.1.1/data/databases/
     * Then run: javac Migrate.java
     * java Migrate
     */
    static String PROTEINS_PATH =
            "/home/palash/Desktop/graph_db/Proteins/Proteins";
    static String IGRAPH_PATH = "/home/palash/Desktop/graph_db/iGraph";
    BatchInserter bi;
    String neo4j_database_folder_path =
            "/home/palash/Desktop/neo4j-community-3.1.1/data/databases/";
    HashMap<Integer, Long> nodes = new HashMap<>();

    public static void main(String[] args) {
        Migrate migrate_object = new Migrate();
        System.out.println("Creating igraph databases...");
        System.out.println("\tCreating graph: igraph.human");
        migrate_object.extract_data_igraph(IGRAPH_PATH + "/human.igraph");
        System.out.println("\tCreating graph: igraph.yeast");
        migrate_object.extract_data_igraph(IGRAPH_PATH + "/yeast.igraph");
//        System.out.println("\nCreating Proteins databases...");
//        migrate_object.extract_data_proteins(PROTEINS_PATH + "/target");
    }

    File extract_data_igraph(String file_path) {
        /**
         * Given the file path, starts reading the graph file for igraphs and
         * creates the database, nodes and relationships based on what is read.
         * @param file_path: The path to the igraph file.
         * @param is_query: True if task is to create a temporary query
         *                   database. Used only for naive subgraph matching
         *                   to create a query database.
         * */
        FileInputStream file_inp_stream = null;
        File file = new File(file_path);
        String line = null;
        File db = new File(neo4j_database_folder_path + "/igraph." + file
                .getName
                        ());
        try {
            bi = BatchInserters.inserter(db);
            file_inp_stream = new FileInputStream(file);
            BufferedReader br = new BufferedReader(new InputStreamReader
                    (file_inp_stream));
            while ((line = br.readLine()) != null) {
                String[] line_arr = line.split(" ");
                if (line_arr[0].equals("t")) {
                    System.out.println("\t\tNew graph with id: " + line_arr[2]);
                    nodes.clear();
                } else if (line_arr[0].equals("v")) {
                    create_new_node_igraph(line_arr);
                } else if (line_arr[0].equals("e")) {
                    create_relationship_igraph(line_arr);
                }
            }
            br.close();
            bi.shutdown();
            compute_and_save_profile(db);
            file_inp_stream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(2);
        }
        return db;
    }

    File extract_data_igraph(String file_path, boolean is_query, int
            graph_num) {
        /**
         * Given the file path, starts reading the graph file for igraphs and
         * creates the database, nodes and relationships based on what is read.
         * @param file_path: The path to the igraph file.
         * @param is_query: True if task is to create a temporary query
         *                   database. Used only for naive subgraph matching
         *                   to create a query database.
         * @param graph_num: If specified, only create the specified graph
         *                       number from the query file. For e.g: to
         *                       create t # 10 from the query file,
         *                       graph_num = 10
         * */
        FileInputStream file_inp_stream = null;
        File file = new File(file_path);
        String line = null;
        File db = new File(neo4j_database_folder_path + "/igraph." + file
                .getName
                        ());
        try {
            bi = BatchInserters.inserter(db);
            file_inp_stream = new FileInputStream(file);
            BufferedReader br = new BufferedReader(new InputStreamReader
                    (file_inp_stream));
            boolean already_created = false;
            while ((line = br.readLine()) != null) {
                String[] line_arr = line.split(" ");
                if (line_arr[0].equals("t")) {
                    if (already_created) {
                        break;
                    }
                    nodes.clear();
                    if (is_query && line_arr[2].equals(graph_num + "")) {
                        System.out.println("Creating graph num: " + graph_num);
                        already_created = true;
                    }
                } else if (line_arr[0].equals("v") && already_created) {
                    create_new_node_igraph(line_arr);
                } else if (line_arr[0].equals("e") && already_created) {
                    create_relationship_igraph(line_arr);
                }
            }
            br.close();
            bi.shutdown();
            compute_and_save_profile(db);
            file_inp_stream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(2);
        }
        return db;
    }


    void create_new_node_igraph(String[] line_arr) {
        /**
         * Creates a new node for igraph.
         * @param line_arr: Given the line from the file, create a node of
         *                the read id and labels.
         * */
        Label[] labels_list = new Label[line_arr.length - 2];
        long node_id = Integer.parseInt(line_arr[1]);
        for (int count = 2; count < line_arr.length; count++) {
            labels_list[count - 2] = Label.label(line_arr[count]);
        }
        bi.createNode(node_id, new HashMap(), labels_list);
    }

    void create_relationship_igraph(String[] line_arr) {
        /**
         * Creates a new relationship for igraph.
         * @param line_arr: Given the line from the file, create a
         *                relationship between the 2 nodes from the line_arr.
         * */
        long from_node_id = Integer.parseInt(line_arr[1]);
        long to_node_id = Integer.parseInt(line_arr[2]);
        String edge_label = line_arr[3];
        bi.createRelationship(from_node_id, to_node_id, RelationshipType
                .withName(edge_label), new HashMap());
    }

    void extract_data_proteins(String path) {
        /**
         * Creates the proteins databases by reading the file.
         * @param path: The path to the proteins target graph file.
         * */
        System.out.println("\tCreating " + path + " databases...");
        File sub_directory = new File(path + "/");
        for (File file : sub_directory.listFiles()) {
            extract_data_from_proteins_file(file);
        }
    }

    File extract_data_from_proteins_file(File file) {
        /**
         * Given the file, processes the file and creates databases.
         * @param file: The File object of the target graph.
         * */
        File db = new File(neo4j_database_folder_path + "/proteins." + file
                .getName());
        try {
            bi = BatchInserters.inserter(db);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("\t\tProcessing: " + file.getAbsolutePath() + " " +
                "into" +
                " database " + db.getAbsolutePath());
        process_file(file.getAbsolutePath());
        bi.shutdown();
//        compute_and_save_profile
//                (new File("/home/palash/Desktop/neo4j-community-3.1.1/data" +
//                        "/databases/proteins." + file.getName()));
        return db;
    }


    void process_file(String file_path) {
        /**
         * Reads the file and creates nodes and relationships based on each
         * line of the file.
         **/
        FileInputStream file_inp_stream = null;
        try {
            file_inp_stream = new FileInputStream(file_path);
            BufferedReader br = new BufferedReader(new InputStreamReader
                    (file_inp_stream));
            String line;
            int num_of_nodes = Integer.parseInt(br.readLine());
            while ((line = br.readLine()) != null) {
                if (line.contains("#") || line.split(" ").length == 0) {
                    continue;
                }
                if (num_of_nodes > 0) {
                    long node_id = Integer.parseInt(line.split(" ")[0]);
                    String attribute = line.split(" ")[1];
                    bi.createNode(node_id, new HashMap(), Label.label
                            (attribute));
                    num_of_nodes -= 1;
                } else {
                    int num_of_edges = Integer.parseInt(line);
                    for (int count_edge = 0; count_edge < num_of_edges;
                         count_edge++) {
                        String[] edge_line = br.readLine().split(" ");
                        long from = Integer.parseInt(edge_line[0]);
                        long to = Integer.parseInt(edge_line[1]);
                        bi.createRelationship(from, to, RelationshipType
                                .withName(""), new HashMap());
                    }
                }
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void compute_and_save_profile(File db) {
        /**
         * Called after each graph is created. Used to compute the profiles
         * and save them as an attribute.
         * */
        HashMap<Node, ArrayList> profiles = new HashMap();
        GraphDatabaseService db_service = new GraphDatabaseFactory()
                .newEmbeddedDatabaseBuilder(db)
                .setConfig(GraphDatabaseSettings.pagecache_memory, "512M")
                .setConfig(GraphDatabaseSettings.string_block_size, "60")
                .setConfig(GraphDatabaseSettings.array_block_size, "300")
                .newGraphDatabase();
        Transaction tx = db_service.beginTx();
        ResourceIterable<Node> nodes = db_service.getAllNodes();
        for (Node node : nodes) {
            ArrayList<String> profile = get_profile_of_node(node);
            profiles.put(node, profile);
        }
        tx.close();
        db_service.shutdown();
        try {
            bi = BatchInserters.inserter(db);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Node node : profiles.keySet()) {
            bi.setNodeProperty(node.getId(), "profile", profiles.get(node)
                    .toString().replace("{", "").replace("}", "").replace("[",
                            "").replace("]", ""));
        }
        bi.shutdown();
    }

    ArrayList<String> get_profile_of_node(Node node) {
        /**
         * Helper function to fetch all label of a node and compute its
         * profiles by fetching the labels of the node's direct neighbors.
         * */
        ArrayList<String> profile = new ArrayList<>();
        Iterable<Label> self_labels = node.getLabels();
        for (Label label : self_labels) {
            profile.add(label.name());
        }
        Iterable<Relationship> relationships = node.getRelationships();
        for (Relationship relationship : relationships) {
            Node neighbor = relationship.getOtherNode(node);
            Iterable<Label> neighbor_labels = neighbor.getLabels();
            for (Label label : neighbor_labels) {
                profile.add(label.name());
            }
        }
        return profile;
    }
}
