import org.apache.commons.io.FileUtils;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 23/3/17 3:47 PM
 * CSCI729_Graph_Databases
 * Run Insturctions:
 */
public class SubgraphMatchProfile {
    /**
     *
     * */
    static String PROTEINS_PATH =
            "/home/palash/Desktop/graph_db/Proteins/Proteins";
    static String IGRAPH_PATH = "/home/palash/Desktop/graph_db/iGraph";
    String neo4j_database_folder_path =
            "/home/palash/Desktop/neo4j-community-3.1.1/data/databases/";
    String query_file_path;
    File query_db_file;
    File target_db_file;
    GraphDatabaseService query_db;
    GraphDatabaseService target_db;
    HashMap<Node, ArrayList<Node>> search_space;
    ArrayList<Node> search_order;
    HashMap<Node, Node> solution;
    Iterable<Node> query_nodes;
    int query_nodes_size;
    int query_size;
    int num_of_solutions_gt;
    int limit;
    File gt_file;

    SubgraphMatchProfile(String target_db_path, String query_file_path, int
            limit) {
        /**
         * Constructor for class
         * @param target_db_path: The path to the database folder.
         * @param query_file_path: The path to the query file.
         * @param limit: The number of solutions to find.
         * */
        this.query_file_path = query_file_path;
        this.limit = limit;
        if(target_db_path.contains("proteins")) {
            query_size = Integer.parseInt(query_file_path.substring
                    (query_file_path.indexOf(".") + 1, query_file_path.indexOf("" +

                            ".sub" +
                            ".grf")));
            gt_file = new File(PROTEINS_PATH + "/ground_truth/Proteins" +
                    "." + query_size + ".gtr");
        }
        target_db_file = new File(target_db_path);
        target_db = new GraphDatabaseFactory()
                .newEmbeddedDatabaseBuilder(target_db_file)
                .setConfig(GraphDatabaseSettings.pagecache_memory, "1024M")
                .setConfig(GraphDatabaseSettings.string_block_size, "60")
                .setConfig(GraphDatabaseSettings.array_block_size, "300")
                .newGraphDatabase();
        search_space = new HashMap<>();
        search_order = new ArrayList<>();
        solution = new HashMap<>();
    }

    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        System.out.println("Enter the path of the target db file: ");
//        String target_db_path = sc.nextLine();
//        System.out.println("Enter the path of the query db file: ");
//        String query_db_path = sc.nextLine();
        String target_db_path =
                "/home/palash/Desktop/neo4j-community-3.1.1/data/databases" +
                        "/proteins.backbones_1DS3.grf";
        String query_file_path =
                "/home/palash/Desktop/graph_db/Proteins/Proteins" +
                        "/query/backbones_1EMA.8.sub.grf";
        SubgraphMatchProfile matcher = new SubgraphMatchProfile
                (target_db_path, query_file_path, 1000);
        matcher.match(1);
    }

    private void create_query_db_in_memory(String query_file_path, int
            graph_num) {
        /**
         * Creates the query file and loads it into memory to make
         * computations faster. Also, this database is deleted at the end of
         * the execution
         * */
        System.out.println("=================================================");
        System.out.println("Creating query database...");
        Migrate migrate_object = new Migrate();
        if (query_file_path.contains("igraph")) {
            query_db_file = migrate_object.extract_data_igraph
                    (query_file_path, true, graph_num);
        } else {
            query_db_file = migrate_object.extract_data_from_proteins_file
                    (new File(query_file_path));
        }
        query_db = new GraphDatabaseFactory()
                .newEmbeddedDatabaseBuilder(query_db_file)
                .setConfig(GraphDatabaseSettings.pagecache_memory, "512M")
                .setConfig(GraphDatabaseSettings.string_block_size, "60")
                .setConfig(GraphDatabaseSettings.array_block_size, "300")
                .newGraphDatabase();
    }

    void match(int graph_number) {
        /**
         * Function that computes the search space, searcch order and calls
         * the backktrack search function.
         * */
        create_query_db_in_memory(query_file_path, graph_number);
        System.out.println("Computing search space...");
        long start_time_full = System.currentTimeMillis();
        compute_search_space();
        System.out.println("\tTime taken to compute search space: " + (System
                .currentTimeMillis() - start_time_full) + " ms");
        long start_time_optimize_space = System.currentTimeMillis();
        optimize_search_space_using_profiles();
        System.out.println("\tTime taken to optimize search space: " + (System
                .currentTimeMillis() - start_time_optimize_space) + " ms");
        long start_time_order = System.currentTimeMillis();
        compute_search_order();
        System.out.println("\tTime taken to compute search order: " + (System
                .currentTimeMillis() - start_time_order) + " ms");
        System.out.println("Searching for subgraph...");
        System.out.println
                ("\n==========================================================");
        long start_time_search = System.currentTimeMillis();
        backtrack_search(0, solution);
        System.out.println
                ("==========================================================");
        System.out.println("\nTime taken to perform naive subgraph matching " +
                "with optimizations to search space: " +
                (System.currentTimeMillis() - start_time_search) + " ms");
        System.out.println("\nTotal time taken: " + (System.currentTimeMillis()
                - start_time_full) + " ms.");
        target_db.shutdown();
        try {
            System.out.println("\nDeleting temporary query database...");
            query_db.shutdown();
            FileUtils.forceDelete(query_db_file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Finished.");
        System.out.println("=================================================");
        System.out.println("\n\n");
    }

    void compute_search_space() {
        /**
         * Computes the search space based on the labels of each query and
         * target nodes.
         * */
        Transaction tx_query = query_db.beginTx();
        query_nodes = query_db.getAllNodes();
        query_nodes_size = 0;
        for (Node node : query_nodes) {
            query_nodes_size += 1;
        }
        for (Node node : query_nodes) {
            ArrayList<String> query_node_labels = get_label(node);
            ArrayList<Node> matched_target_nodes = new ArrayList<>();
            for (String query_node_label : query_node_labels) {
                Transaction tx_target = target_db.beginTx();
                ResourceIterator<Node> target_nodes = target_db.findNodes
                        (Label.label(query_node_label));
                for (ResourceIterator<Node> it = target_nodes; it.hasNext(); ) {
                    Node target_node = it.next();
                    matched_target_nodes.add(target_node);
                }
                tx_target.close();
            }
            search_space.put(node, matched_target_nodes);
        }
        tx_query.close();
    }

    void optimize_search_space_using_profiles() {
        System.out.println("Optimizing search space using profiles...");
        for (Node query_node : search_space.keySet()) {
            Transaction tx_query = query_db.beginTx();
            String query_node_profile = String.valueOf(query_node.getProperty
                    ("profile"));
            ArrayList<Node> matched_nodes = search_space.get(query_node);
            ArrayList<Node> new_matched_nodes = new ArrayList<>();
            for (Node target_node : matched_nodes) {
                Transaction tx_target = target_db.beginTx();
                String target_node_profile = String.valueOf(target_node
                        .getProperty("profile"));
                if (_check_if_profiles_equal(query_node_profile,
                        target_node_profile)) {
                    new_matched_nodes.add(target_node);
                }
                tx_target.close();
            }
            tx_query.close();
            search_space.put(query_node, new_matched_nodes);
        }
    }

    private boolean _check_if_profiles_equal(String query_node_profile,
                                             String target_node_profile) {
        HashMap<String, Integer> query_node_labels_count = new HashMap<>();
        for (String label : query_node_profile.split(", ")) {
            if (!query_node_labels_count.containsKey(label)) {
                query_node_labels_count.put(label, 0);
            }
            query_node_labels_count.put(label, query_node_labels_count.get
                    (label) + 1);
        }
        HashMap<String, Integer> target_node_labels_count = new HashMap<>();
        for (String label : target_node_profile.split(", ")) {
            if (!target_node_labels_count.containsKey(label)) {
                target_node_labels_count.put(label, 0);
            }
            target_node_labels_count.put(label, target_node_labels_count.get
                    (label) + 1);
        }
        for (String label : query_node_labels_count.keySet()) {
            if (!target_node_labels_count.containsKey(label) &&
                    target_node_labels_count.get(label) !=
                            query_node_labels_count.get(label)) {
                return false;
            }
        }
        return true;
    }

    void compute_search_order() {
        /**
         * Computes the search order in a DFS style.
         * */
        System.out.println("Computing search order...");
        ArrayList<Node> failed_check_nodes = new ArrayList<>();
        for (Node query_node : search_space.keySet()) {
            if (search_order.size() == 0) {
                search_order.add(query_node);
            } else {
                boolean to_add = check_if_nodes_connected(query_node);
                if (to_add) {
                    search_order.add(query_node);
                } else {
                    failed_check_nodes.add(query_node);
                }
            }
        }
        for (Node query_node_failed_check : failed_check_nodes) {
            search_order.add(query_node_failed_check);
        }
    }

    void backtrack_search(int i, HashMap<Node, Node> solution) {
        /**
         * Function that computes the solution by doing a backtrack search
         * recursively.
         * */
        if(limit <= 0){
            return;
        }
        if (solution.size() == query_nodes_size) {
            limit -= 1;
            System.out.println("Computed solution: " + solution.toString());
            if(query_file_path.contains("Proteins")) {
                compare_solution_and_ground_truth();
            }
        } else {
            Node u = search_order.get(i);
            for (Node v : search_space.get(u)) {
                if (!solution.values().contains(v)) {
                    if (can_map(u, v, solution)) {
                        solution.put(u, v);
                        backtrack_search(i + 1, solution);
                        solution.remove(u);
                    }
                }
            }
        }
    }

    boolean can_map(Node u, Node v, HashMap<Node, Node> solution_param) {
        /**
         * Check function that checks an edge from the query and target
         * graphs.
         * */
        Transaction tx_query = query_db.beginTx();
        Transaction tx_target = target_db.beginTx();
        boolean res = true;
        for (Node query_node : query_nodes) {
            if (query_node.getId() == u.getId()) {
                continue;
            }
            if (solution_param.containsKey(query_node)) {
                for (Relationship rel : query_node.getRelationships()) {
                    if (rel.getOtherNode(query_node).getId() == u.getId()) {
                        boolean exists = false;
                        for (Relationship rel2 : v.getRelationships()) {
                            if (rel2.getOtherNode(v).getId() == solution_param
                                    .get(query_node).getId()) {
                                exists = true;
                                break;
                            }
                        }
                        res = exists;
                    }
                }
            }
        }
        tx_query.close();
        tx_target.close();
        return res;
    }

    private void compare_solution_and_ground_truth() {
        /**
         * If the query and target databases are of type proteins, this
         * function checks each solution with the ground truth file.
         * */
        FileInputStream file_inp_stream = null;
        int num_of_pairs_acc_to_gt = -1;
        ArrayList<String> solutions_gt = new ArrayList<>();
        try {
            file_inp_stream = new FileInputStream(gt_file);
            BufferedReader br = new BufferedReader(new InputStreamReader
                    (file_inp_stream));
            String line;
            while ((line = br.readLine()) != null) {
                String[] query_file_name = query_file_path.split("/");
                if (line.equals("T:" + target_db_file.getName().replace
                        ("proteins.", "")) && br.readLine().equals
                        ("P:" + query_file_name[query_file_name.length - 1])) {
                    num_of_solutions_gt = Integer.parseInt(br.readLine()
                            .split(":")[1]);

                    for (int count = 0; count < num_of_solutions_gt; count++) {
                        solutions_gt.add(br.readLine());
                    }
                }
            }
            file_inp_stream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (String sol : solutions_gt) {
            boolean solution_verified = false;
            String solution_string = "";
            HashMap<String, String> parsed_sol = parse_solution_string_gt
                    (sol, num_of_pairs_acc_to_gt);
            for (Node key : solution.keySet()) {
                if (parsed_sol.containsKey(key.getId() + "") && parsed_sol
                        .get(key
                        .getId() + "").equals(solution.get(key).getId() + "")) {
                    solution_verified = true;
                    solution_string = sol;
                } else {
                    solution_verified = false;
                    solution_string = "";
                    break;
                }
            }
            if (solution_verified) {
                System.out.println("Solution verified. Line from Ground Truth" +
                        " " +
                        "file " + gt_file.getName() + ": " + solution_string
                        + "\n");
                break;
            }
        }

    }

    private HashMap<String, String> parse_solution_string_gt(String sol, int
            num_of_pairs) {
        /**
         * Helper function to parse the solution from the ground truth file.
         * */
        HashMap<String, String> solution = new HashMap();
        String solution_format = sol.substring(sol.replace("S:","").indexOf
                (":")+3, sol.length());
        String[] mappings = solution_format.split(";");
        for (String map : mappings) {
            String[] nodes = map.split(",");
            solution.put(nodes[0], nodes[1]);
        }
        return solution;
    }

    private boolean check_if_nodes_connected(Node query_node1) {
        Transaction tx = query_db.beginTx();
        for (Node query_node2 : search_order) {
            Iterable<Relationship> relationships =
                    query_node1.getRelationships();
            for (Relationship relationship : relationships) {
                if (query_node2.getId() == relationship.getOtherNode
                        (query_node1).getId
                        ()) {
                    return true;
                }
            }
        }
        tx.close();
        return false;
    }

    ArrayList<String> get_label(Node node) {
        /**
         * Helper function to fetch the labels from the database for a
         * particular node.
         * */
        ArrayList<String> label_names = new ArrayList<>();
        Iterable<Label> labels = node.getLabels();
        for (Label label : labels) {
            label_names.add(label.name());
        }
        return label_names;
    }
}
