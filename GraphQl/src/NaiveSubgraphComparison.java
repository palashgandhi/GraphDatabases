import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 29/3/17 2:18 PM
 * CSCI729_Graph_Databases
 */
public class NaiveSubgraphComparison {

    public static final String DASHES = new String(new char[220]).replace("\0",
            "-");
    static String PROTEINS_PATH =
            "/home/palash/Desktop/graph_db/Proteins/Proteins";
    static String IGRAPH_PATH = "/home/palash/Desktop/graph_db/iGraph";
    String neo4j_database_folder_path =
            "/home/palash/Desktop/neo4j-community-3.1.1/data/databases/";
    File ground_truth_folder;
    File proteins_folder;


    NaiveSubgraphComparison() {
        proteins_folder = new File(PROTEINS_PATH);
        ground_truth_folder = new File(PROTEINS_PATH + "/ground_truth");
    }

    public static void main(String[] args) {
        NaiveSubgraphComparison compare_object = new NaiveSubgraphComparison();

        // EDIT THIS ==============================================>
        compare_object.perform_comparison("proteins");
//        compare_object.perform_comparison("igraph.human");
    }

    private static int get_query_size_from_file(String gt_file_name) {
        return Integer.parseInt(gt_file_name.
                replace("Proteins.", "").replace(".gtr", ""));
    }

    void perform_comparison(String type) {
        /**
         * Begins performing the 3 different types of naive subgraph matching.
         * */
        // EDIT THIS ==============================================>
        int query_number = 3;
        ArrayList<String> files = new ArrayList<>();
        if(type.contains("igraph")){
            // EDIT THIS ==============================================>
            files.add("/home/palash/Desktop/neo4j-community-3.1.1/data/databases" +
                    "/igraph.human.igraph");
            files.add("/home/palash/Desktop/graph_db/iGraph/human_q10.igraph");
            System.out.println("\n\n\n\n" + DASHES);
            System.out.println(" Using files:" + files
                    .toString());
            System.out.println("\n\nPerforming naive subgraph " +
                    "matching...");
            perform_naive_subgraph_matching(files, query_number);
            System.out.println("\n\nPerforming naive subgraph " +
                    "matching with optimized search space using " +
                    "profiles...");
            perform_naive_subgraph_matching_with_profiles(files, query_number);
            System.out.println("\n\nPerforming naive subgraph " +
                    "matching with optimized search space using " +
                    "profiles and optimized search order using " +
                    "cost models...");
            perform_naive_subgraph_matching_profiles_optimized_order
                    (files, query_number);
            System.out.println(DASHES);
        }
        else if(type.equals("proteins")) {
            for (File gt_file : ground_truth_folder.listFiles()) {
                String gt_file_name = gt_file.getName();
                int query_size = get_query_size_from_file(gt_file_name);
                // EDIT THIS ==============================================>
                if (query_size == 32) {
                    System.out.println("Beginning....");
                    int type_count = 1;
                    for (String type_proteins : new String[]{"backbones",
                            "bos_taurus",
                            "ecoli", "human", "mus_musculus", "rattus_norvegicus",

                            "saccharomyces_cerevisiae"}) {
                        files = process_file_for_type(type_proteins,
                                gt_file);
                        System.out.println("\n\n\n\n" + DASHES);
                        System.out.println("(" + type_count + "/7) For query " +
                                "size:" +
                                query_size + "" + " using files:" + files
                                .toString
                                ());
                        type_count += 1;
                        System.out.println("\n\nPerforming naive subgraph " +
                                "matching...");
                        perform_naive_subgraph_matching(files, 0);
                        System.out.println("\n\nPerforming naive subgraph " +
                                "matching with optimized search space using " +
                                "profiles...");
                        perform_naive_subgraph_matching_with_profiles(files,
                                0);
                        System.out.println("\n\nPerforming naive subgraph " +
                                "matching with optimized search space using " +
                                "profiles and optimized search order using " +
                                "cost models...");
                        perform_naive_subgraph_matching_profiles_optimized_order
                                (files, 0);
                        System.out.println(DASHES);
                    }
                }
            }
        }
    }

    private void perform_naive_subgraph_matching_profiles_optimized_order
            (ArrayList<String> files, int graph_num) {
        String target_db_path = files.get(0);
        String query_file = files.get(1);
        SubgraphMatchOptimizedOrder naive_matcher = new
                SubgraphMatchOptimizedOrder
                (target_db_path, query_file, 100);
        naive_matcher.match(graph_num);
        naive_matcher.query_db.shutdown();
        naive_matcher.target_db.shutdown();
    }

    private void perform_naive_subgraph_matching_with_profiles
            (ArrayList<String> files, int graph_num) {
        String target_db_path = files.get(0);
        String query_file = files.get(1);
        SubgraphMatchProfile naive_matcher = new SubgraphMatchProfile
                (target_db_path, query_file, 100);
        naive_matcher.match(graph_num);
        naive_matcher.target_db.shutdown();
    }

    private void perform_naive_subgraph_matching(ArrayList<String> files, int graph_num) {
        String target_db_path = files.get(0);
        String query_file = files.get(1);
        NaiveSubgraphMatcher naive_matcher = new NaiveSubgraphMatcher
                (target_db_path, query_file, 100);
        naive_matcher.match();
        naive_matcher.target_db.shutdown();
    }

    private ArrayList<String> process_file_for_type(String type, File gt_file) {
        String target_graph_file = null;
        String query_graph_file = null;
        ArrayList<String> files = new ArrayList<>();
        try {
            FileInputStream fis = new FileInputStream(gt_file);
            BufferedReader br = new BufferedReader(new InputStreamReader
                    (fis));
            String line;
            while ((line = br.readLine()) != null) {
                if (line.contains("T:") && line.contains(type)) {
                    String pattern_line = br.readLine();
                    target_graph_file = neo4j_database_folder_path +
                            "/proteins." + line.substring(2, line.length());
                    query_graph_file = PROTEINS_PATH + "/query/" + pattern_line.substring(2, pattern_line
                            .length
                                    ());
                    files.add(target_graph_file);
                    files.add(query_graph_file);
                    break;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return files;
    }
}
