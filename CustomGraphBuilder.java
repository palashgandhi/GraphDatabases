import java.io.File;
import java.util.HashMap;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.unsafe.batchinsert.BatchInserter;
import org.neo4j.unsafe.batchinsert.BatchInserters;

/**
 * @author Palash Gandhi
 * @date 11/26/17. GraphDatabases
 */
public class CustomGraphBuilder {


  CustomGraphBuilder() {

  }

  void build_query_graph() {
    File db = new File(
        "/home/pbg4930/Desktop/neo4j-community-3.1.1/data/databases/custom_query_graph.grf");
    try {
      BatchInserter bi = BatchInserters.inserter(db);
      Label[] labels_list = new Label[1];

      labels_list[0] = Label.label("A");
      bi.createNode(1, new HashMap(), labels_list);

      labels_list[0] = Label.label("D");
      bi.createNode(2, new HashMap(), labels_list);

      labels_list[0] = Label.label("C");
      bi.createNode(3, new HashMap(), labels_list);

      labels_list[0] = Label.label("B");
      bi.createNode(4, new HashMap(), labels_list);

      labels_list[0] = Label.label("E");
      bi.createNode(5, new HashMap(), labels_list);

      labels_list[0] = Label.label("A");
      bi.createNode(6, new HashMap(), labels_list);

      labels_list[0] = Label.label("B");
      bi.createNode(7, new HashMap(), labels_list);

      bi.createRelationship(1, 2, RelationshipType.withName(""), new HashMap<>());
      bi.createRelationship(1, 4, RelationshipType.withName(""), new HashMap<>());
      bi.createRelationship(1, 7, RelationshipType.withName(""), new HashMap<>());

      bi.createRelationship(2, 3, RelationshipType.withName(""), new HashMap<>());

      bi.createRelationship(3, 4, RelationshipType.withName(""), new HashMap<>());

      bi.createRelationship(4, 2, RelationshipType.withName(""), new HashMap<>());

      bi.createRelationship(7, 6, RelationshipType.withName(""), new HashMap<>());

      bi.createRelationship(6, 5, RelationshipType.withName(""), new HashMap<>());

      bi.shutdown();
    } catch (Exception e) {
      e.printStackTrace();
      System.exit(2);
    }
  }

  void build_target_graph() {
    File db = new File(
        "/home/pbg4930/Desktop/neo4j-community-3.1.1/data/databases/custom_target_graph.grf");
    try {
      BatchInserter bi = BatchInserters.inserter(db);
      Label[] labels_list = new Label[1];

      labels_list[0] = Label.label("A");
      bi.createNode(1, new HashMap(), labels_list);

      labels_list[0] = Label.label("C");
      bi.createNode(2, new HashMap(), labels_list);

      labels_list[0] = Label.label("A");
      bi.createNode(3, new HashMap(), labels_list);

      labels_list[0] = Label.label("D");
      bi.createNode(4, new HashMap(), labels_list);

      labels_list[0] = Label.label("C");
      bi.createNode(5, new HashMap(), labels_list);

      labels_list[0] = Label.label("B");
      bi.createNode(6, new HashMap(), labels_list);

      labels_list[0] = Label.label("D");
      bi.createNode(7, new HashMap(), labels_list);

      labels_list[0] = Label.label("B");
      bi.createNode(9, new HashMap(), labels_list);

      labels_list[0] = Label.label("A");
      bi.createNode(10, new HashMap(), labels_list);

      labels_list[0] = Label.label("E");
      bi.createNode(15, new HashMap(), labels_list);

      create_relationship(bi, 1, 4);
      create_relationship(bi, 1, 6);
      create_relationship(bi, 1, 9);
      create_relationship(bi, 2, 1);
      create_relationship(bi, 4, 5);
      create_relationship(bi, 5, 6);
      create_relationship(bi, 6, 4);
      create_relationship(bi, 9, 7);
      create_relationship(bi, 9, 10);
      create_relationship(bi, 7, 10);
      create_relationship(bi, 10, 15);
      create_relationship(bi, 15, 9);
      create_relationship(bi, 15, 3);

      bi.shutdown();
    } catch (Exception e) {
      e.printStackTrace();
      System.exit(2);
    }
  }

  private void create_relationship(BatchInserter bi, int i, int i1) {
    bi.createRelationship(i, i1, RelationshipType.withName(""), new HashMap<>());
  }

  public static void main(String[] args) {
    CustomGraphBuilder builder = new CustomGraphBuilder();
    builder.build_query_graph();
    builder.build_target_graph();
  }
}
