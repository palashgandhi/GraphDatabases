import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;

/**
 * @author Palash Gandhi
 * @date 10/28/17. GraphDatabases
 */

public class SubgraphMatchingAnalyzer {

  String[] algorithms;
  HashMap<String, AnalysisArchive> solutions;

  HashMap<String, ArrayList<Long>> search_space_times;
  HashMap<String, ArrayList<Long>> solution_times;

  static DatabaseHelpers helper;

  SubgraphMatchingAnalyzer(String[] args) {
    algorithms = args;
    solutions = new HashMap<>();
    helper = new DatabaseHelpers();
    search_space_times = new HashMap<>();
    solution_times = new HashMap();
  }

  public static void main(String[] args) {
    System.out.println("Arguments: " + Arrays.toString(args));
    SubgraphMatchingAnalyzer analyzer = new SubgraphMatchingAnalyzer(args);
    FileDatabaseService file_helper = new FileDatabaseService(
        "/home/pbg4930/Desktop/neo4j-community-3.1.1/data/databases/");

    String target_file_path = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/target/backbones_1EMA.grf";
    File query_file_folder = new File("/home/pbg4930/Desktop/graph_db/Proteins/Proteins/query/");
    int[] sizes = new int[]{64, 128, 256};
    for (Integer size : sizes) {
      System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\nSize: " + size);
      for (File query_file : query_file_folder.listFiles()) {
        if (!query_file.getName().contains(size + ".sub.grf")) {
          continue;
        }
        String ground_truth_file_path = get_ground_truth_file(query_file);
        GraphDatabaseService query_db = file_helper.create_db(query_file.getAbsolutePath());
        GraphDatabaseService target_db = file_helper.create_db(target_file_path);

        Transaction tx_q = query_db.beginTx();
        Transaction tx_g = target_db.beginTx();
        System.out.println("Query graph: " + query_db);
        System.out.println("Query graph size: " + helper.get_all_nodes(query_db).size());
        System.out.println("Target graph size: " + helper.get_all_nodes(target_db).size());
        try {
          HashMap<String, AnalysisArchive> solutions = analyzer
              .match(query_db, target_db, query_file.getAbsolutePath(), target_file_path,
                  ground_truth_file_path);
          analyzer.analyze_solutions(solutions);
        } catch (Exception e) {

        }
        System.out.println("All search space times: " + analyzer.search_space_times);
        System.out.println("All search times: " + analyzer.solution_times);
        tx_q.close();
        tx_g.close();
        query_db.shutdown();
        target_db.shutdown();
        System.out.println("\n");
      }
    }
  }

  private static String get_ground_truth_file(File query_file) {
    String gtr_name = "";
    if (query_file.getName().contains(".8.sub.grf")) {
      gtr_name = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/ground_truth/Proteins.8.gtr";
    } else if (query_file.getName().contains(".16.sub.grf")) {
      gtr_name = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/ground_truth/Proteins.16.gtr";
    } else if (query_file.getName().contains(".32.sub.grf")) {
      gtr_name = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/ground_truth/Proteins.32.gtr";
    } else if (query_file.getName().contains(".64.sub.grf")) {
      gtr_name = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/ground_truth/Proteins.64.gtr";
    } else if (query_file.getName().contains(".128.sub.grf")) {
      gtr_name = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/ground_truth/Proteins.128.gtr";
    } else if (query_file.getName().contains(".256.sub.grf")) {
      gtr_name = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/ground_truth/Proteins.256.gtr";
    }
    return gtr_name;
  }

  private void analyze_solutions(HashMap<String, AnalysisArchive> solutions) {
    System.out
        .println("\n\n=========================================================================");
    for (String algorithm : solutions.keySet()) {
      AnalysisArchive archive = solutions.get(algorithm);
      System.out.println("-------------------------------------------------------------------");
      System.out.println("Analyzing " + algorithm);
      System.out.println(algorithm + " found " + archive.solutions.size() + " solutions.");
      System.out.println("Analysis: " + archive);
      System.out.println("-----------------------------------------------------------------------");
      if (!search_space_times.containsKey(algorithm)) {
        search_space_times.put(algorithm, new ArrayList<>());
        solution_times.put(algorithm, new ArrayList<>());
      }
      ArrayList<Long> search_space_temp = search_space_times.get(algorithm);
      ArrayList<Long> sol_times_temp = solution_times.get(algorithm);
      search_space_temp.add(archive.search_space_time);
      sol_times_temp.add(archive.solution_time);
      search_space_times.put(algorithm, search_space_temp);
      solution_times.put(algorithm, sol_times_temp);
    }
    System.out.println("=========================================================================");

    System.out.println("search_space_times: ");
    System.out.println(search_space_times);
    System.out.println("Solution times: ");
    System.out.println(solutions);
  }

  private HashMap<String, AnalysisArchive> match(GraphDatabaseService query_db,
      GraphDatabaseService target_db, String query_file_path, String target_file_path,
      String ground_truth_file_path) {
    for (String arg : algorithms) {
      System.out.println("Algorithm: " + arg);
      AnalysisArchive archive = new AnalysisArchive();
      if (arg.equals("turboiso")) {
        archive = execute_and_analyze_turboiso(query_file_path, target_file_path,
            ground_truth_file_path, query_db, target_db);
      } else if (arg.equals("sqbc")) {
        archive = execute_and_analyze_sqbc(query_file_path, target_file_path,
            ground_truth_file_path, query_db, target_db);
      } else if (arg.equals("naive")) {
        archive = execute_and_analyze_naive(query_file_path, target_file_path,
            ground_truth_file_path, query_db, target_db);
      } else if (arg.equals("vf2_plus")) {
        archive = execute_and_analyze_vf2_plus(query_file_path, target_file_path,
            ground_truth_file_path, query_db, target_db);
      } else if (arg.equals("boostiso")) {
        archive = execute_and_analyze_boostiso(query_file_path, target_file_path,
            ground_truth_file_path, query_db, target_db);
      } else {
        System.out.println("\n\nCannot recognize arg: " + arg + "\n\n");
        System.exit(2);
      }
      solutions.put(arg, archive);
    }
    return solutions;
  }

  private AnalysisArchive execute_and_analyze_naive(String query_file_path, String target_file_path,
      String ground_truth_file_path, GraphDatabaseService query_db,
      GraphDatabaseService target_db) {
    NaiveSubgraphMatcher matcher = new NaiveSubgraphMatcher(query_file_path, target_file_path,
        ground_truth_file_path, 1000000);
    AnalysisArchive archive_naive = matcher.match(query_db, target_db);
    return archive_naive;
  }

  private AnalysisArchive execute_and_analyze_turboiso(String query_file_path,
      String target_file_path, String ground_truth_file_path, GraphDatabaseService query_db,
      GraphDatabaseService target_db) {
    TurboISO matcher = new TurboISO(query_db, target_db);
    AnalysisArchive turbo_iso_analysis = matcher
        .match(query_file_path, target_file_path, ground_truth_file_path);
    return turbo_iso_analysis;
  }

  private AnalysisArchive execute_and_analyze_sqbc(String query_file_path, String target_file_path,
      String ground_truth_file_path, GraphDatabaseService query_db,
      GraphDatabaseService target_db) {
    SQBC matcher = new SQBC();
    AnalysisArchive archive = matcher
        .match(query_file_path, target_file_path, ground_truth_file_path, query_db, target_db);
    return archive;
  }

  private AnalysisArchive execute_and_analyze_vf2_plus(String query_file_path,
      String target_file_path, String ground_truth_file_path, GraphDatabaseService query_db,
      GraphDatabaseService target_db) {
    VF2Plus matcher = new VF2Plus(query_file_path, target_file_path, ground_truth_file_path,
        query_db, target_db);
    AnalysisArchive archive = matcher.match();
    return archive;
  }

  private AnalysisArchive execute_and_analyze_boostiso(String query_file_path,
      String target_file_path, String ground_truth_file_path, GraphDatabaseService query_db,
      GraphDatabaseService target_db) {
    BoostISO matcher = new BoostISO(query_file_path, target_file_path, ground_truth_file_path,
        query_db, target_db);
    AnalysisArchive archive = matcher.match(query_db, target_db);
    return archive;
  }
}


