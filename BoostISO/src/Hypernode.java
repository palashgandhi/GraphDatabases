import java.util.ArrayList;
import java.util.HashSet;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.RelationshipType;

/**
 * Class representing a hypernode that is used by BoostISO.
 *
 * @author Palash Gandhi
 * @date 12/13/17. GraphDatabases
 */
class Hypernode {

  HashSet<Node> nodes;
  boolean is_clique;
  ArrayList<String> labels;
  Node hypernode;
  ArrayList<Hypernode> qdc_children;
  int num_of_qdc_parents;
  ArrayList<Hypernode> qde_list;

  /**
   * Initializes fields for a new hypernode to be created.
   */
  Hypernode(GraphDatabaseService database) {
    is_clique = false;
    nodes = new HashSet();
    labels = new ArrayList<>();
    hypernode = database.createNode(Label.label("ignore_hypernode"));
    qdc_children = new ArrayList<>();
    num_of_qdc_parents = 0;
    qde_list = new ArrayList<>();
  }


  /**
   * Adds a node to this hypernode.
   *
   * @param v The node to be added.
   */
  public void add(Node v) {
    nodes.add(v);
    DatabaseHelpers helpers = new DatabaseHelpers();
    for (String label : helpers.get_labels(v)) {
      if (!labels.contains(label)) {
        labels.add(label);
      }
    }
    hypernode.createRelationshipTo(v, RelationshipType.withName("ignore_hypernode_relationship"));
  }

  /**
   * Override toString method for debugging.
   *
   * @return toString custom string.
   */
  public String toString() {
    return "[   " + hypernode.toString() + ": " + nodes.toString() + "   ]";
  }
}
