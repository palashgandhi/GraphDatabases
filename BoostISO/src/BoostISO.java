import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;

/**
 * This class implements the BoostISO algorithm on top of a naive approach for subgraph isomorphism.
 * It takes a neo4j query database and a neo4j target database and finds all solutions.
 *
 * @author Palash Gandhi
 * @date 11/8/17. GraphDatabases
 */
public class BoostISO {

  GraphDatabaseService query_db;
  GraphDatabaseService target_db;

  private String query_file_path;
  private String target_file_path;
  private String ground_truth_file_path;

  DatabaseHelpers helpers;
  private HashMap<Node, ArrayList<Hypernode>> search_space;
  private ArrayList<Node> search_order;
  private HashMap<Node, Hypernode> hypernodeMap;

  AnalysisArchive archive;

  /**
   * Constructor for class. Initializes required fields.
   *
   * @param query_file_path String object that represents the query file in txt format from which
   * the database is built.
   * @param target_file_path String object that represents the target file in txt format from which
   * the database is built.
   * @param ground_truth_file_path String object that represents the graound truth file in txt
   * format that is used to verify the results.
   * @param query_database org.neo4j.graphdb.GraphDatabaseService object for query database.
   * @param target_database org.neo4j.graphdb.GraphDatabaseService object for target database.
   */
  BoostISO(String query_file_path, String target_file_path, String ground_truth_file_path,
      GraphDatabaseService query_database, GraphDatabaseService target_database) {
    query_db = query_database;
    target_db = target_database;
    this.query_file_path = query_file_path;
    this.target_file_path = target_file_path;
    this.ground_truth_file_path = ground_truth_file_path;
    helpers = new DatabaseHelpers();
    search_space = new HashMap<>();
    search_order = new ArrayList<>();
    archive = new AnalysisArchive();
  }

  public static void main(String[] args) {

    FileDatabaseService file_helper = new FileDatabaseService(
        "/home/pbg4930/Desktop/neo4j-community-3.1.1/data/databases/");

    String query_file_path = "";
    String target_file_path = "";
    String ground_truth_file_path = "";
    GraphDatabaseService query_db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(new File(
        "/home/pbg4930/Desktop/neo4j-community-3.1.1/data/databases/custom_query_graph.grf")).
        setConfig(GraphDatabaseSettings.pagecache_memory, "512M").
        setConfig(GraphDatabaseSettings.string_block_size, "60").
        setConfig(GraphDatabaseSettings.array_block_size, "300").
        newGraphDatabase();
    GraphDatabaseService target_db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(new File(
        "/home/pbg4930/Desktop/neo4j-community-3.1.1/data/databases/custom_target_graph.grf")).
        setConfig(GraphDatabaseSettings.pagecache_memory, "512M").
        setConfig(GraphDatabaseSettings.string_block_size, "60").
        setConfig(GraphDatabaseSettings.array_block_size, "300").
        newGraphDatabase();

//    query_file_path = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/query/backbones_1EMA.8.sub.grf";
//    target_file_path = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/target/backbones_1EMA.grf";
//    ground_truth_file_path = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/ground_truth/Proteins.8.gtr";
//    query_db = file_helper.create_db(query_file_path);
//    target_db = file_helper.create_db(target_file_path);
    try {
      BoostISO matcher = new BoostISO(query_file_path, target_file_path, ground_truth_file_path,
          query_db, target_db);
      matcher.match(query_db, target_db);
    } catch (Exception e) {
      System.out.println(e);
      e.printStackTrace();
    }
  }

  /**
   * Computes the adapted hypergraph of a given neo4j database.
   *
   * @param database GraphDatabaseService representing the database.
   */
  public void compute_adapted_hypergraph(GraphDatabaseService database) {

    Set<String> label_set = new HashSet<>();
    Set<Hypernode> hypernode_set = new HashSet<>();
    hypernodeMap = new HashMap<>();

    target_db.getAllLabels().forEach(label -> label_set.add(label.name()));

    ArrayList<Node> visited = new ArrayList<>();
    for (Node v : target_db.getAllNodes()) {
      if (visited.contains(v)) {
        continue;
      }
      visited.add(v);
      Hypernode h = new Hypernode(database);

      h.add(v);
      hypernodeMap.put(v, h);

      hypernode_set.add(h);

      for (Node v_prime : helpers.get_one_step_reachable(v, database)) {
        if (!helpers.get_labels(v_prime).equals(helpers.get_labels(v)) || v.getId() == v_prime
            .getId()) {
          continue;
        }
        if (is_syntactically_equivalent(v, v_prime, target_db)) {
          h.is_clique = true;
          h.add(v_prime);
          hypernodeMap.put(v_prime, h);
          visited.add(v_prime);
        }
      }

      if (!h.is_clique) {
        for (Node v_prime : helpers.get_two_step_reachable(v, database)) {
          if (!helpers.get_labels(v_prime).equals(helpers.get_labels(v)) || v.getId() == v_prime
              .getId()) {
            continue;
          }
          if (is_syntactically_equivalent(v, v_prime, database)) {
            h.add(v_prime);
            hypernodeMap.put(v_prime, h);
            visited.add(v_prime);
          }
        }
      }
    }

    for (Relationship rel : database.getAllRelationships()) {
      Node start = rel.getStartNode();
      Node end = rel.getEndNode();
      Hypernode h = hypernodeMap.get(start);
      Hypernode h_prime = hypernodeMap.get(end);
      if (h != null && h_prime != null && !h.equals(h_prime)) {
        h.hypernode.createRelationshipTo(h_prime.hypernode,
            RelationshipType.withName("ignore_hypernode_relationship"));
      }
    }

    for (Hypernode h : hypernode_set) {
      ArrayList<Hypernode> neighbor_set = _get_neighbor_set(h, hypernodeMap);
      for (Hypernode h_prime : neighbor_set) {
        if (is_syntactically_contained(h, h_prime, target_db)) {
          h.hypernode.createRelationshipTo(h_prime.hypernode,
              RelationshipType.withName("ignore_hypernode_relationship"));
        }
      }
    }
  }

  /**
   * Main routine to be called that makes use of all naive methods with BoostISO optimizations to
   * find solutions.
   *
   * @param query_db org.neo4j.graphdb.GraphDatabaseService object for query database.
   * @param target_db org.neo4j.graphdb.GraphDatabaseService object for target database.
   */
  public AnalysisArchive match(GraphDatabaseService query_db, GraphDatabaseService target_db) {
    Transaction tx_tgt = target_db.beginTx();
    Transaction tx_query = query_db.beginTx();
    compute_adapted_hypergraph(target_db);
    long start_time = System.currentTimeMillis();
    compute_search_space();
    archive.search_space_time = System.currentTimeMillis() - start_time;
//    build_drt();
    compute_search_order();
    start_time = System.currentTimeMillis();
    subgraph_search(query_db, target_db, new HashMap<>(), 0);
    archive.solution_time = System.currentTimeMillis() - start_time;
    tx_tgt.close();
    tx_query.close();
    return archive;
  }

  /**
   * Builds the Dynamic Relationship Table defined by BoostISO. This method needs work.
   */
  private void build_drt() {
    for (Node u : search_space.keySet()) {
      ArrayList<Hypernode> filtered_candidates = search_space.get(u);
      for (int i = 0; i < filtered_candidates.size(); i++) {
        Hypernode h = filtered_candidates.get(i);
        for (int j = i; j < filtered_candidates.size(); j++) {
          Hypernode h_prime = filtered_candidates.get(j);
          if (is_query_dependent_equivalent(u, h, h_prime)) {
            h.qde_list.add(h_prime);
            filtered_candidates.remove(h_prime);
          }
        }
      }
      search_space.put(u, filtered_candidates);
      for (Hypernode h : search_space.get(u)) {
        for (Hypernode h_prime : search_space.get(u)) {
          if (h.equals(h_prime)) {
            continue;
          }
          if (is_query_dependent_contained(u, h, h_prime)) {
            h.qdc_children.add(h_prime);
            h_prime.num_of_qdc_parents += 1;
          } else if (is_query_dependent_contained(u, h_prime, h)) {
            h.qdc_children.add(h_prime);
            h.num_of_qdc_parents += 1;
          }
        }
      }
    }
  }

  /**
   * Computes the search space based on the labels of each query and
   * target nodes.
   */
  void compute_search_space() {
    Transaction tx_query = query_db.beginTx();
    Transaction tx_target = target_db.beginTx();
    for (Node query_node : helpers.get_all_nodes(query_db)) {
      ArrayList<Hypernode> matched_target_nodes = new ArrayList<>();
      for (Node target_node : helpers.get_all_nodes(target_db)) {
        Hypernode h = hypernodeMap.get(target_node);
        if (h.labels.containsAll(helpers.get_labels(query_node)) && get_sc_parents(h).size() == 0) {
          matched_target_nodes.add(h);
        }
      }
      search_space.put(query_node, matched_target_nodes);
    }
    tx_query.close();
    tx_target.close();
  }

  /**
   * Computes the search order in a DFS style. To be optimized. Dirty way of computing the order.
   */
  void compute_search_order() {
    Transaction tx_query = query_db.beginTx();
    Transaction tx_target = target_db.beginTx();
    ArrayList<Node> failed_check_nodes = new ArrayList<>();
    Node start_node = get_node_with_smallest_search_space();
    search_order.add(start_node);
    for (Node query_node : search_space.keySet()) {
      boolean to_add = check_if_nodes_connected(query_node);
      if (to_add && !search_order.contains(query_node)) {
        search_order.add(query_node);
      } else {
        failed_check_nodes.add(query_node);
      }
    }
    for (Node query_node_failed_check : failed_check_nodes) {
      if (!search_order.contains(query_node_failed_check)) {
        search_order.add(query_node_failed_check);
      }
    }
    tx_query.close();
    tx_target.close();
  }

  /**
   * Selects the start node.
   *
   * @return Node with the smallest search space.
   */
  private Node get_node_with_smallest_search_space() {
    int min_size_search_space = -1;
    Node start_node = null;
    for (Node query_node : search_space.keySet()) {
      if (min_size_search_space == -1
          || search_space.get(query_node).size() < min_size_search_space) {
        min_size_search_space = search_space.get(query_node).size();
        start_node = query_node;
      }
    }
    return start_node;
  }

  /**
   * To be modified/removed. Part of dirty search order computation.
   */
  private boolean check_if_nodes_connected(Node query_node1) {
    for (Node query_node2 : search_order) {
      Iterable<Relationship> relationships = query_node1.getRelationships();
      for (Relationship relationship : relationships) {
        if (query_node2.getId() == relationship.getOtherNode(query_node1).getId()) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Main recursive subroutine that performs the backtracking.
   *
   * @param query_db GraphDatabaseService object representing the query database.
   * @param target_db GraphDatabaseService object representing the target database.
   * @param solution Current mapping between query node and hypernode.
   * @param i Current index to be checked.
   * @return boolean
   */
  public boolean subgraph_search(GraphDatabaseService query_db, GraphDatabaseService target_db,
      HashMap<Node, Hypernode> solution, int i) {
    boolean flag = false;
    if (solution.size() == helpers.get_all_nodes(query_db).size()) {
      finalEG(query_db, target_db, solution);
      Node n = search_order.get(search_order.size() - 1);
      ArrayList<Hypernode> hypernodes = dynamicCL(search_space.get(n), solution.get(n));
      search_space.put(n, hypernodes);
    } else {
      Node u = search_order.get(i);
      for (Hypernode h : search_space.get(u)) {
        if (is_joinable(query_db, target_db, solution, h, u)) {
          solution.put(u, h);
          if (subgraph_search(query_db, target_db, solution, i + 1)) {
            flag = true;
          }
          solution.remove(u);
        }
      }
    }

    if (flag) {
      Node u_prime = search_order.get(i - 1);
      ArrayList<Hypernode> hypernodes = dynamicCL(search_space.get(u_prime), solution.get(u_prime));
      search_space.put(u_prime, hypernodes);
    }
    compute_search_space();
    return flag;
  }

  /**
   * Dynamic candidate loading as defined by BoostISO.
   *
   * @param hypernodes List of hypernodes.
   * @param h Current hypernode in consideration.
   * @return Filtered candidates.
   */
  private ArrayList<Hypernode> dynamicCL(ArrayList<Hypernode> hypernodes, Hypernode h) {
    ArrayList<Hypernode> sc_children = get_sc_children(h);
    HashMap<Hypernode, Integer> ump = new HashMap<>();
    for (Hypernode h_prime : sc_children) {
      if (!ump.containsKey(h_prime)) {
        ump.put(h_prime,
            helpers.get_neighbors(h_prime.hypernode, target_db, Direction.INCOMING).size());
      }
      ump.put(h_prime, ump.get(h_prime) - 1);
      if (ump.get(h_prime) == 0) {
        hypernodes.add(h_prime);
      }
    }
    return hypernodes;
  }

  /**
   * Gets the SC children for a given hypernode.
   *
   * @param h Hypernode object for which the SC children are to be fetched.
   * @return List of SC children (Hypernodes)
   */
  private ArrayList<Hypernode> get_sc_children(Hypernode h) {
    ArrayList<Hypernode> sc_children = new ArrayList<>();
    for (Node neighbor : helpers.get_neighbors(h.hypernode, target_db, Direction.OUTGOING)) {
      if (helpers.get_labels(neighbor).contains("ignore_hypernode") && hypernodeMap
          .containsKey(neighbor)) {
        sc_children.add(hypernodeMap.get(neighbor));
      }
    }
    return sc_children;
  }

  /**
   * Needs rework after build_drt fixed.
   *
   * @param query_db GraphDatabaseService object representing the query database.
   * @param target_db GraphDatabaseService object representing the target database.
   * @param solution Current mapping between query node and hypernode.
   */
  private void finalEG(GraphDatabaseService query_db, GraphDatabaseService target_db,
      HashMap<Node, Hypernode> solution) {
//    solutions.add(solution);
    System.out.println(solution);
    for (Node u : helpers.get_all_nodes(query_db)) {
      ArrayList<Node> r = new ArrayList<>();

    }
  }

  /**
   * Revised ISJOINABLE method defined by BoostISO.
   *
   * @param query_graph GraphDatabaseService object representing the query database.
   * @param target_adapted_graph GraphDatabaseService object representing the target database with
   * the adapted graph within it.
   * @param solution Current mapping between query node and hypernode.
   * @param h Current hypernode to check.
   * @param u Current query node in consideration.
   * @return boolean
   */
  public boolean is_joinable(GraphDatabaseService query_graph,
      GraphDatabaseService target_adapted_graph, HashMap<Node, Hypernode> solution, Hypernode h,
      Node u) {
    for (Node query_node : helpers.get_all_nodes(query_graph)) {
      if (!solution.containsKey(query_node)) {
        continue;
      }
      if (solution.containsKey(query_node) && !solution.get(query_node).equals(h)) {
        if (helpers.get_neighbors(query_node, query_graph, Direction.BOTH).contains(u) && !helpers
            .get_neighbors(solution.get(query_node).hypernode, target_adapted_graph, Direction.BOTH)
            .contains(h.hypernode)) {
          return false;
        }
      } else {
        if (helpers.get_neighbors(query_node, query_graph, Direction.BOTH).contains(u)
            && !h.is_clique) {
          return false;
        }
        if (get_used_times(h, solution) >= h.nodes.size()) {
          return false;
        }
      }
    }
    return true;
  }

  /**
   * Returns the number of times the hypernode is used in the current mapping.
   *
   * @param h Current hypernode.
   * @param solution Current mapping between query node and hypernode.
   * @return int
   */
  private int get_used_times(Hypernode h, HashMap<Node, Hypernode> solution) {
    int used_times = 0;
    for (Hypernode h_prime : solution.values()) {
      if (h.equals(h_prime)) {
        used_times += 1;
      }
    }
    return used_times;
  }

  private ArrayList<Hypernode> _get_neighbor_set(Hypernode h,
      HashMap<Node, Hypernode> hypernodeMap) {
    ArrayList<Hypernode> neighbor_set = new ArrayList<>();
    ArrayList<Node> prospects = helpers.get_neighbors(h.hypernode, target_db, Direction.BOTH);
    prospects.retainAll(helpers.get_two_step_reachable(h.hypernode, target_db));
    for (Node neighbor : prospects) {
      ArrayList<String> hypernode_labels = helpers.get_labels(neighbor);
      if (hypernode_labels.contains("ignore_hypernode") && hypernode_labels
          .containsAll(helpers.get_labels(h.hypernode)) && hypernodeMap.containsKey(neighbor)) {
        neighbor_set.add(hypernodeMap.get(neighbor));
      }
    }
    return neighbor_set;
  }

  /**
   * Method that checks if 2 nodes are syntactically equivalent.
   *
   * @param node1 Node object
   * @param node2 Node object
   * @param db The database in which the nodes exist.
   * @return boolean
   */
  private boolean is_syntactically_equivalent(Node node1, Node node2, GraphDatabaseService db) {
    ArrayList<Node> adj_node1 = helpers.get_neighbors(node1, db, Direction.BOTH);
    ArrayList<Node> adj_node2 = helpers.get_neighbors(node2, db, Direction.BOTH);
    adj_node1.remove(node2);
    adj_node2.remove(node1);
    if (helpers.get_labels(node1).equals(helpers.get_labels(node2)) && adj_node1
        .equals(adj_node2)) {
      return true;
    }
    return false;
  }

  /**
   * Method that checks if 2 hypernodes are syntactically contained.
   *
   * @param h Hypernode object.
   * @param h_prime Hypernode object.
   * @param db The database in which the hypernodes exist.
   * @return boolean
   */
  private boolean is_syntactically_contained(Hypernode h, Hypernode h_prime,
      GraphDatabaseService db) {
    ArrayList<Node> adj_node1 = helpers.get_neighbors(h.hypernode, db, Direction.BOTH);
    ArrayList<Node> adj_node2 = helpers.get_neighbors(h_prime.hypernode, db, Direction.BOTH);
    adj_node1.remove(h);
    adj_node2.remove(h_prime);
    if (helpers.get_labels(h.hypernode).equals(helpers.get_labels(h_prime.hypernode))
        && adj_node1.size() <= adj_node2.size()) {
      return true;
    }
    return false;
  }

  /**
   * Method that checks if 2 hypernodes are query dependent contained, given a query node.
   *
   * @param u Current query node.
   * @param h_prime Hypernode object.
   * @param h Hypernode object.
   * @return boolean
   */
  private boolean is_query_dependent_contained(Node u, Hypernode h_prime, Hypernode h) {
    ArrayList<Hypernode> qdn_h = get_query_dependent_neighbors(h, u);
    ArrayList<Hypernode> qdn_h_prime = get_query_dependent_neighbors(h_prime, u);
    qdn_h.remove(h_prime);
    qdn_h_prime.remove(h);
    if (helpers.get_labels(h_prime.hypernode).equals(helpers.get_labels(h.hypernode))
        && qdn_h_prime.size() <= qdn_h.size()) {
      return true;
    }
    return false;
  }

  /**
   * Method that checks if 2 hypernodes are query dependent equivalent, given a query node.
   *
   * @param u Current query node.
   * @param h_prime Hypernode object.
   * @param h Hypernode object.
   * @return boolean
   */
  private boolean is_query_dependent_equivalent(Node u, Hypernode h, Hypernode h_prime) {
    ArrayList<Hypernode> qdn_h = get_query_dependent_neighbors(h, u);
    ArrayList<Hypernode> qdn_h_prime = get_query_dependent_neighbors(h_prime, u);
    qdn_h.remove(h_prime);
    qdn_h_prime.remove(h);
    if (h_prime.labels.equals(h.labels) && qdn_h.size() == qdn_h_prime.size()) {
      return true;
    }
    return false;
  }

  /**
   * Computes the query dependent neighbors of the hypernode, given a query node.
   *
   * @param h Hypernode for which the QDN is to be computed.
   * @param u The query node in consideration
   * @return List of QDN hypernodes.
   */
  private ArrayList<Hypernode> get_query_dependent_neighbors(Hypernode h, Node u) {
    ArrayList<Hypernode> qdn = new ArrayList<>();
    for (Node neighbor : helpers.get_neighbors(h.hypernode, target_db, Direction.BOTH)) {
      if (helpers.get_labels(neighbor).contains("ignore_hypernode") && !h.labels
          .equals(helpers.get_labels(u))) {
        continue;
      }
      boolean found = false;
      for (Node query_neighbor : helpers.get_neighbors(u, query_db, Direction.BOTH)) {
        if (helpers.get_labels(neighbor).containsAll(helpers.get_labels(query_neighbor))) {
          found = true;
          break;
        }
      }
      if (found) {
        qdn.add(hypernodeMap.get(neighbor));
      }
    }
    return qdn;
  }

  /**
   * Returns the SC parents of a hypernode.
   *
   * @param h The hypernode which sc parents are to be computed.
   * @return List of nodes that are sc parents.
   */
  public ArrayList<Node> get_sc_parents(Hypernode h) {
    ArrayList<Node> sc_parents = new ArrayList<>();
    for (Node node : helpers.get_neighbors(h.hypernode, target_db, Direction.INCOMING)) {
      if (hypernodeMap.values().contains(node)) {
        sc_parents.add(node);
      }
    }
    return sc_parents;
  }
}
