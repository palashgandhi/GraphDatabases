import java.util.Arrays;
import org.neo4j.graphdb.Node;

/**
 * Class that represents an SQBC sequence and defines operations on these sequences.
 *
 * @author Palash Gandhi
 * @date 12/13/17. GraphDatabases
 */
class SQBCSequence {

  Node[] sequence;
  int size;

  /**
   * Initializes a sequence of the specified size.
   *
   * @param size int representing the size of the sequence.
   */
  SQBCSequence(int size) {
    this.sequence = new Node[size];
    this.size = 0;
  }

  /**
   * Adds a node to the sequence.
   *
   * @param node The node to be added.
   */
  public void add(Node node) {
    this.sequence[size] = node;
    this.size += 1;
  }

  /**
   * Sets the node at the specified position in the sequence.
   *
   * @param node The node to be set.
   * @param position Position at which the node is to be set.
   */
  public void set(Node node, int position) {
    this.sequence[position] = node;
    this.size += 1;
  }

  /**
   * Removes the specified node from the sequence.
   *
   * @param node The node to be removed.
   */
  public void remove(Node node) {
    for (int index = 0; index < this.sequence.length; index++) {
      if (this.sequence[index] != null && this.sequence[index].equals(node)) {
        this.sequence[index] = null;
        this.size -= 1;
      }
    }
  }

  /**
   * Returns the index of the specified node from the sequence.
   *
   * @param node The node of which the index is to be returned.
   * @return int representing the index.
   */
  public int get_index_of(Node node) {
    for (int index = 0; index < this.sequence.length; index++) {
      if (this.sequence[index] != null && this.sequence[index].equals(node)) {
        return index;
      }
    }
    return -1;
  }

  /**
   * Checks if the sequence contains the specified node.
   *
   * @param node The node to be found.
   * @return boolean
   */
  public boolean contains(Node node) {
    for (int index = 0; index < this.sequence.length; index++) {
      if (this.sequence[index] != null && this.sequence[index].equals(node)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Returns the node at the specified position.
   *
   * @param position Position in sequence.
   * @return Node that exists at the specified position.
   */
  public Node get(int position) {
    return this.sequence[position];
  }

  /**
   * To string override.
   */
  public String toString() {
    return Arrays.toString(this.sequence);
  }
}
