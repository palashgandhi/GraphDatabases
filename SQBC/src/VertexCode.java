import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

/**
 * Class that handles the construction and fetch operations of the vertex codes for SQBC.
 *
 * @author Palash Gandhi
 * @date 9/13/17. GraphDatabases
 */
class VertexCode {

  long node_id;
  ArrayList<String> labels;
  ArrayList<Node> largest_maximal_clique_containing_vertex;
  int largest_maximal_clique_containing_vertex_size;
  HashMap<String, ArrayList<Integer>> neighborhood_degree_info_labels;
  DatabaseHelpers helper = new DatabaseHelpers();

  /**
   * Method that builds the first part of the SQBC index i.e the vertex code for the passed node.
   *
   * @param vertex The neo4j node for which the vertex needs to be built.
   * @param cliques The list of all cliques in the database.
   * @param db GraphDatabaseService object representing the neo4j database.
   */
  public void construct_and_set_index_in_neo4j(Node vertex, ArrayList<ArrayList<Node>> cliques,
      GraphDatabaseService db) {
    node_id = vertex.getId();
    largest_maximal_clique_containing_vertex = find_max_clique(vertex, cliques);
    largest_maximal_clique_containing_vertex_size = largest_maximal_clique_containing_vertex.size();
    if (largest_maximal_clique_containing_vertex.size() <= 2) {
      largest_maximal_clique_containing_vertex_size = 0;
    }
    neighborhood_degree_info_labels = get_neighborhood_degree_info_labels(vertex);
    set_index_in_neo4j(vertex);
  }

  /**
   * Helper method that stores the index information within the node itself as attributes. All
   * indexes are stored as attributes starting with "ignore_index_sqbc".
   *
   * @param vertex The node for which the index needs to be set.
   */
  private void set_index_in_neo4j(Node vertex) {
    long[] clique_nodes_ids = new long[largest_maximal_clique_containing_vertex_size];
    for (int count = 0; count < largest_maximal_clique_containing_vertex_size; count++) {
      clique_nodes_ids[count] = largest_maximal_clique_containing_vertex.get(count).getId();
    }
    vertex.setProperty("ignore_index_sqbc_clique_containing_node", clique_nodes_ids);
    for (String neighbor : neighborhood_degree_info_labels.keySet()) {
      ArrayList<Integer> ndl = neighborhood_degree_info_labels.get(neighbor);
      Integer[] ndl_info = ndl.toArray(new Integer[ndl.size()]);
      vertex.setProperty("ignore_index_sqbc_ndl_" + neighbor, ndl_info);
    }
  }

  /**
   * Gets the index from a neo4j node. All indexes are stored as attributes starting with
   * "ignore_index_sqbc".
   *
   * @param vertex The node for which the index needs to be fetched.
   * @param db GraphDatabaseService object representing the neo4j database.
   */
  public void get_index_from_neo4j(Node vertex, GraphDatabaseService db) {
    largest_maximal_clique_containing_vertex = new ArrayList<>();
    neighborhood_degree_info_labels = new HashMap<>();
    labels = helper.get_labels(vertex);
    for (String property : vertex.getPropertyKeys()) {
      if (property.equals("ignore_index_sqbc_clique_containing_node")) {
        long[] clique_containing_vertex = (long[]) vertex.getProperty(property);
        for (long node_id : clique_containing_vertex) {
          largest_maximal_clique_containing_vertex.add(db.getNodeById(node_id));
        }
        largest_maximal_clique_containing_vertex_size = largest_maximal_clique_containing_vertex
            .size();
      } else if (property.contains("ignore_index_sqbc_ndl_")) {
        String neighbor = property.substring(
            property.indexOf("ignore_index_sqbc_ndl_") + "ignore_index_sqbc_ndl_".length(),
            property.length());
        int[] ndl_info = (int[]) vertex.getProperty(property);
        ArrayList<Integer> ndl_list = new ArrayList<>();
        for (Integer d : ndl_info) {
          ndl_list.add(d);
        }
        neighborhood_degree_info_labels.put(neighbor, ndl_list);
      }
    }
  }

  /**
   * Helper method to compute the NDL information of a vertex.
   *
   * @param vertex The node of which the NDL is to be computed.
   * @return HashMap sturcture representing the NDL info.
   */
  public HashMap<String, ArrayList<Integer>> get_neighborhood_degree_info_labels(Node vertex) {
    HashMap<String, ArrayList<Integer>> neighborhood_degree_info_labels = new HashMap();
    ArrayList<Node> visited = new ArrayList<>();
    for (Relationship rel : vertex.getRelationships()) {
      Node neighbor = rel.getOtherNode(vertex);
      if (visited.contains(neighbor)) {
        continue;
      }
      visited.add(neighbor);
      int degree = neighbor.getDegree();
      ArrayList<String> labels = helper.get_labels(neighbor);
      for (String label : labels) {
        if (!neighborhood_degree_info_labels.containsKey(label)) {
          neighborhood_degree_info_labels.put(label, new ArrayList());
        }
        ArrayList<Integer> degree_list = neighborhood_degree_info_labels.get(label);
        degree_list.add(degree);
        neighborhood_degree_info_labels.put(label, degree_list);
      }
    }
    for (String ndl_key : neighborhood_degree_info_labels.keySet()) {
      ArrayList<Integer> val = new ArrayList<>(neighborhood_degree_info_labels.get(ndl_key));
      Collections.sort(val);
      neighborhood_degree_info_labels.put(ndl_key, val);
    }
    return neighborhood_degree_info_labels;
  }

  /**
   * Helper function that finds the maximal clique that a vertex belongs to, given a list of
   * cliques.
   *
   * @param vertex The node for which the maximal clique is to be found.
   * @param cliques The list of lists of nodes, each of which is a clique.
   * @return The clique in the form of a list of nodes.
   */
  private ArrayList<Node> find_max_clique(Node vertex, ArrayList<ArrayList<Node>> cliques) {
    ArrayList<Node> largest_clique = new ArrayList<>();
    for (ArrayList<Node> clique : cliques) {
      if (clique.contains(vertex) && largest_clique.size() < clique.size()) {
        largest_clique = clique;
      }
    }
    return largest_clique;
  }

  /**
   * Override toString method for debugging.
   *
   * @return toString custom string.
   */
  public String toString() {
    return "ID: " + node_id + " => [" + labels + ", "
        + largest_maximal_clique_containing_vertex_size + ", " + neighborhood_degree_info_labels
        .toString() + "]";
  }
}
