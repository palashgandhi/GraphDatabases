import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 23/3/17 3:47 PM CSCI729_Graph_Databases Run Insturctions:
 */
public class NaiveSubgraphMatcher {

  /**
   *
   * */
  String query_file_path;
  String target_file_path;
  String ground_truth_file_path;
  GraphDatabaseService query_db;
  GraphDatabaseService target_db;
  HashMap<Node, ArrayList<Node>> search_space;
  ArrayList<Node> search_order;
  HashMap<Node, Node> solution;
  Iterable<Node> query_nodes;
  int query_nodes_size;
  int limit;
  AnalysisArchive archive;
  DatabaseHelpers helper = new DatabaseHelpers();


  NaiveSubgraphMatcher(String query_file_path, String target_file_path,
      String ground_truth_file_path, int limit) {
    /**
     * Constructor for class
     * @param target_db_path: The path to the database folder.
     * @param query_file_path: The path to the query file.
     * @param limit: The number of solutions to find.
     * */
    this.query_file_path = query_file_path;
    this.target_file_path = target_file_path;
    this.ground_truth_file_path = ground_truth_file_path;
    this.limit = limit;
    search_space = new HashMap<>();
    search_order = new ArrayList<>();
    solution = new HashMap<>();
    archive = new AnalysisArchive();
  }

  public static void main(String[] args) {
    String query_file_path = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/query/backbones_1EMA.8.sub.grf";
    String target_file_path = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/target/backbones_1EMA.grf";
    String ground_truth = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/ground_truth/Proteins.8.gtr";
    FileDatabaseService file_helper = new FileDatabaseService(
        "/home/pbg4930/Desktop/neo4j-community-3.1.1/data/databases/");
    GraphDatabaseService query_db = file_helper.create_db(query_file_path);
    GraphDatabaseService target_db = file_helper.create_db(target_file_path);
    NaiveSubgraphMatcher matcher = new NaiveSubgraphMatcher(query_file_path, target_file_path,
        ground_truth, 1000);
//    GraphDatabaseService target_db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(new File(
//        "/home/pbg4930/Desktop/neo4j-community-3.1.1/data/databases/igraph.0.yeast.igraph")).
//        setConfig(GraphDatabaseSettings.pagecache_memory, "512M").
//        setConfig(GraphDatabaseSettings.string_block_size, "60").
//        setConfig(GraphDatabaseSettings.array_block_size, "300").
//        newGraphDatabase();
//    GraphDatabaseService query_db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(
//        new File("/home/pbg4930/Desktop/neo4j-community-3.1.1/data/databases/yeast.palash.grf")).
//        setConfig(GraphDatabaseSettings.pagecache_memory, "512M").
//        setConfig(GraphDatabaseSettings.string_block_size, "60").
//        setConfig(GraphDatabaseSettings.array_block_size, "300").
//        newGraphDatabase();
    matcher.match(query_db, target_db);
  }

  AnalysisArchive match(GraphDatabaseService query_db, GraphDatabaseService target_db) {
    /**
     * Function that computes the search space, search order and calls
     * the backtrack search function.
     * */
    this.query_db = query_db;
    this.target_db = target_db;
    Transaction tx_query = query_db.beginTx();
    Transaction tx_target = target_db.beginTx();
    long start_time = System.currentTimeMillis();
    compute_search_space();
    archive.search_space_time = System.currentTimeMillis() - start_time;
    compute_search_order();
    start_time = System.currentTimeMillis();
    try {
      backtrack_search(0, solution);
    } catch (Exception e){

    }
    archive.solution_time = System.currentTimeMillis() - start_time;
    tx_query.close();
    tx_target.close();
    return archive;
  }

  void compute_search_space() {
    /**
     * Computes the search space based on the labels of each query and
     * target nodes.
     * */
    query_nodes = query_db.getAllNodes();
    query_nodes_size = helper.get_all_nodes(query_db).size();
    for (Node node : query_nodes) {
      ArrayList<Node> matched_target_nodes = new ArrayList<>();
      for (Node target_node : target_db.getAllNodes()) {
        if (helper.get_labels(target_node).containsAll(helper.get_labels(node))) {
          matched_target_nodes.add(target_node);
        }
      }
      search_space.put(node, matched_target_nodes);
    }
  }

  void compute_search_order() {
    /**
     * Computes the search order in a DFS style.
     * */
    query_nodes = query_db.getAllNodes();
    ArrayList<Node> failed_check_nodes = new ArrayList<>();
    Node start_node = get_node_with_smallest_search_space();
    search_order.add(start_node);
    for (Node query_node : search_space.keySet()) {
      boolean to_add = check_if_nodes_connected(query_node);
      if (to_add && !search_order.contains(query_node)) {
        search_order.add(query_node);
      } else {
        failed_check_nodes.add(query_node);
      }
    }
    for (Node query_node_failed_check : failed_check_nodes) {
      if (!search_order.contains(query_node_failed_check)) {
        search_order.add(query_node_failed_check);
      }
    }
  }

  private Node get_node_with_smallest_search_space() {
    /**
     * Returns the node with the smallest search space.
     * */
    int min_size_search_space = -1;
    Node start_node = null;
    for (Node query_node : search_space.keySet()) {
      if (min_size_search_space == -1
          || search_space.get(query_node).size() < min_size_search_space) {
        min_size_search_space = search_space.get(query_node).size();
        start_node = query_node;
      }
    }
    return start_node;
  }

  void backtrack_search(int i, HashMap<Node, Node> solution) {
    /**
     * Function that computes the solution by doing a backtrack search
     * recursively.
     * */
    if (limit <= 0) {
      return;
    }
    if (solution.size() == query_nodes_size) {
      limit -= 1;
      archive.solutions.add(solution);
      if (query_file_path.length() > 0 && target_file_path.length() > 0) {
        File query_file_obj = new File(query_file_path);
        File target_file_obj = new File(target_file_path);
        helper.compare_solution_and_ground_truth(ground_truth_file_path, solution,
            query_file_obj.getName(), target_file_obj.getName());
      }
    } else {
      Node u = search_order.get(i);
      for (Node v : search_space.get(u)) {
        if (!solution.values().contains(v)) {
          if (can_map(u, v, solution)) {
            solution.put(u, v);
            backtrack_search(i + 1, solution);
            solution.remove(u);
          }
        }
      }
    }
  }

  boolean can_map(Node u, Node v, HashMap<Node, Node> solution_param) {
    /**
     * Check function that checks an edge from the query and target
     * graphs.
     * */
    boolean res = true;
    for (Node query_node : query_nodes) {
      if (query_node.getId() == u.getId()) {
        continue;
      }
      if (solution_param.containsKey(query_node)) {
        for (Node q_neighbor : helper.get_neighbors(query_node, query_db, Direction.BOTH)) {
          if (q_neighbor.getId() == u.getId()) {
            boolean exists = false;
            for (Node t_neighbor : helper.get_neighbors(v, target_db, Direction.BOTH)) {
              if (t_neighbor.getId() == solution_param.get(query_node).getId()) {
                exists = true;
                break;
              }
            }
            res = exists;
            if (!res) {
              return false;
            }
          }
        }
      }
    }
    return res;
  }

  private boolean check_if_nodes_connected(Node query_node1) {
    /**
     * Checks if a node is connected to any other node in the current
     * search order.
     * */
    for (Node query_node2 : search_order) {
      Iterable<Relationship> relationships = query_node1.getRelationships();
      for (Relationship relationship : relationships) {
        if (query_node2.getId() == relationship.getOtherNode(query_node1).getId()) {
          return true;
        }
      }
    }
    return false;
  }
}
