import com.google.common.collect.Collections2;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;

/**
 * This class implements the SQBC algorithm for subgraph isomorphism. It takes a neo4j query
 * database and a neo4j target database and finds all solutions.
 *
 * @author Palash Gandhi
 * @date 9/4/17. GraphDatabases
 */
public class SQBC {

  private String query_file_path;
  private String target_file_path;
  private String ground_truth_file_path;
  private GraphDatabaseService query_db;
  private GraphDatabaseService target_db;
  ArrayList<ArrayList<Node>> target_cliques;
  ArrayList<ArrayList<Node>> query_cliques;
  private DatabaseHelpers helper;
  private int query_graph_size;
  AnalysisArchive archive;
  private ArrayList<Long> search_space_times;

  /**
   * Constructor for class.
   */
  SQBC() {
    helper = new DatabaseHelpers();
  }

  /**
   * Constructs the 2 part index required by SQBC which consists of the clique and vertex codes for
   * a given neo4j database.
   *
   * @param db GraphDatabaseService object pointing to the neo4j database.
   */
  public void construct_index(GraphDatabaseService db) {
    Transaction tx_query = db.beginTx();
    ArrayList<ArrayList<Node>> cliques = helper.get_all_maximal_cliques(db);
    for (Node node : db.getAllNodes()) {
      VertexCode vertex_code = new VertexCode();
      vertex_code.construct_and_set_index_in_neo4j(node, cliques, db);
    }
    for (ArrayList<Node> clique : cliques) {
      CliqueCode clique_code = new CliqueCode();
      clique_code.construct_and_set_index_in_neo4j(clique);
    }
    tx_query.close();
  }

  /**
   * Main routine to be called that makes use of all TurboISO methods to find solutions.
   *
   * @param query_file_path String object that represents the query file in txt format from which
   * the database is built.
   * @param target_file_path String object that represents the target file in txt format from which
   * the database is built.
   * @param ground_truth_file_path String object that represents the graound truth file in txt
   * format that is used to verify the results.
   * @param query_database org.neo4j.graphdb.GraphDatabaseService object for query database.
   * @param target_database org.neo4j.graphdb.GraphDatabaseService object for target database.
   * @return The analysis archive that contains the solutions and time analysis.
   */
  public AnalysisArchive match(String query_file_path, String target_file_path,
      String ground_truth_file_path, GraphDatabaseService query_database,
      GraphDatabaseService target_database) {
    this.query_file_path = query_file_path;
    this.target_file_path = target_file_path;
    this.ground_truth_file_path = ground_truth_file_path;
    query_db = query_database;
    target_db = target_database;
    archive = new AnalysisArchive();
    Transaction tx_query = query_database.beginTx();
    Transaction tx_target = target_database.beginTx();
    query_graph_size = helper.get_all_nodes(query_database).size();
    target_cliques = helper.get_all_maximal_cliques(target_database);
    query_cliques = helper.get_all_maximal_cliques(query_database);
    SQBCSequence SMQ = get_initial_smq();
    SQBCSequence SMG = new SQBCSequence(query_graph_size);
    int position = 0;
    long start_time = System.currentTimeMillis();
    search_space_times = new ArrayList<>();
    process_query(SMQ, SMG, position);
    archive.solution_time = System.currentTimeMillis() - start_time;

    long avg = 0;
    for (Long time : search_space_times) {
      avg += time;
    }
    archive.search_space_time = (long) (8.0 * (avg / search_space_times.size()));

    tx_query.close();
    tx_target.close();
    return archive;
  }

  /**
   * Generates the initial SMQ sequence that picks the node with the highest degree.
   *
   * @return List containing single node that has the highest degree from the query database.
   */
  private SQBCSequence get_initial_smq() {
    SQBCSequence SMQ = new SQBCSequence(query_graph_size);
    Node node_with_highest_degree = helper.get_node_with_highest_degree(query_db);
    SMQ.add(node_with_highest_degree);
    return SMQ;
  }

  /**
   * Main recursive subroutine that performs the backtracking search to find solutions.
   *
   * @param SMQ Current query vertices in the mapping.
   * @param SMG Corresponding matched target nodes in the mapping.
   * @param position Current position of the SMQ to be considered.
   */
  private void process_query(SQBCSequence SMQ, SQBCSequence SMG, int position) {
    if (check_solution_found(SMQ, SMG) || position >= query_graph_size) {
      return;
    }
    Node current_query_node = SMQ.get(position);
    long start_time = System.currentTimeMillis();
    ArrayList<Node> candidate_vertices_of_current_query_node = get_candidate_vertices(SMQ, SMG,
        current_query_node);
    search_space_times.add(System.currentTimeMillis() - start_time);
    ArrayList<Node> neighbors_of_query_node = helper
        .get_neighbors(current_query_node, query_db, Direction.BOTH);
    for (Node neighbor : neighbors_of_query_node) {
      if (!SMQ.contains(neighbor)) {
        SMQ.add(neighbor);
      }
    }
    for (Node u0 : candidate_vertices_of_current_query_node) {
      ArrayList<Node> clique_containing_u0 = helper.get_clique_containing_node(target_cliques, u0);
      int k = clique_containing_u0.size();
      if (k == 0) {
        if (matches(u0, current_query_node, SMQ, SMG, position) && !SMG.contains(u0)) {
          SMG.set(u0, position);
          process_query(SMQ, SMG, position + 1);
          SMG.remove(u0);
        }
      } else {
        if (matches(u0, current_query_node, SMQ, SMG, position) && !SMG.contains(u0)) {
          SMG.set(u0, position);
          ArrayList<Node> unmatched_clique_nodes = new ArrayList<>(clique_containing_u0);
          unmatched_clique_nodes.removeAll(Arrays.asList(SMG.sequence));
          Collection<List<Node>> combinations = get_all_possible_combinations_from_clique(u0,
              unmatched_clique_nodes);
          for (List<Node> combination : combinations) {
            int new_pos = position;
            boolean possible_match = true;
            for (int index = 0; index < combination.size(); index++) {
              new_pos += 1;
              if (new_pos < SMQ.size) {
                Node query_node = SMQ.get(new_pos);
                ArrayList<Node> combination_list = new ArrayList<>();
                combination_list.addAll(combination);
                Node target_node = combination_list.get(index);
                if (!matches(target_node, query_node, SMQ, SMG, new_pos) || SMG
                    .contains(target_node) || !get_candidate_vertices(SMQ, SMG, query_node)
                    .contains(target_node)) {
                  possible_match = false;
                }
                SMG.set(target_node, SMQ.get_index_of(query_node));
              }
            }
            if (possible_match) {
              process_query(SMQ, SMG, position + combination.size());
            }
            for (Node node : combination) {
              SMG.remove(node);
            }
          }
          process_query(SMQ, SMG, position + 1);
          SMG.remove(u0);
        }
      }
    }
  }

  /**
   * When a node is part of a clique, this method is called to generate all combinations of the
   * nodes in the clique.
   *
   * @param u0 Current node that exists in a clique.
   * @param clique_containing_u0 The clique that contains u0.
   * @return Collection of lists that are combinations of the clique.
   */
  private Collection<List<Node>> get_all_possible_combinations_from_clique(Node u0,
      ArrayList<Node> clique_containing_u0) {
    List<Node> possibilities = new ArrayList(clique_containing_u0);
    possibilities.remove(u0);
    return Collections2.permutations(possibilities);
  }

  /**
   * Uses the SQBC concepts to generate the possible matches of a query node.
   *
   * @param smq Current query nodes in mapping.
   * @param smg Corresponding matched data nodes in the mapping.
   * @param current_query_node Current query node in consideration for which the candidates are
   * generated.
   * @return List of candidate nodes.
   */
  private ArrayList<Node> get_candidate_vertices(SQBCSequence smq, SQBCSequence smg,
      Node current_query_node) {
    ArrayList<Node> candidates = get_super_vertices(current_query_node);
    Node prior_vertex = get_prior_vertex_of_node(smq, smg, current_query_node);
    if (prior_vertex != null) {
      int prior_vertex_index = smq.get_index_of(prior_vertex);
      try {
        Node prior_vertex_match = smg.get(prior_vertex_index);
        ArrayList<Node> neighbors_of_match = helper
            .get_neighbors(prior_vertex_match, target_db, Direction.BOTH);
        candidates.retainAll(neighbors_of_match);
      } catch (Exception e) {
        e.printStackTrace();
        System.exit(2);
      }
    }
    ArrayList<Node> query_clique_match = helper
        .get_clique_containing_node(query_cliques, current_query_node);
    if (query_clique_match.size() > 0) {
      for (ArrayList<Node> target_clique : target_cliques) {
        if (is_subclique(query_clique_match, target_clique)) {
          candidates.retainAll(target_clique);
        }
      }
    }
    return candidates;
  }

  /**
   * Returns the prior vertex of a node.
   *
   * @param smq Current query nodes in mapping.
   * @param smg Corresponding matched data nodes in the mapping.
   * @param current_query_node Current query node in consideration for which the prior vertex is to
   * be found.
   * @return Prior vertex of the node.
   */
  private Node get_prior_vertex_of_node(SQBCSequence smq, SQBCSequence smg,
      Node current_query_node) {
    ArrayList<Node> neighbors = helper.get_neighbors(current_query_node, query_db, Direction.BOTH);
    for (int index = smq.get_index_of(current_query_node); index >= 0; index--) {
      Node node = smq.get(index);
      if (neighbors.contains(node) && smq.get_index_of(node) < smq.get_index_of(current_query_node)
          && smg.get(index) != null) {
        return node;
      }
    }
    return null;
  }

  /**
   * Gets all the super vertices of the query node.
   *
   * @param current_query_node Current query node in consideration for which the super vertices are
   * to be found.
   * @return List of super verteices of the node.
   */
  private ArrayList<Node> get_super_vertices(Node current_query_node) {
    ArrayList<Node> matched_target_nodes = new ArrayList<>();
    for (Node target_node : helper.get_all_nodes(target_db)) {
      VertexCode query_code = new VertexCode();
      query_code.get_index_from_neo4j(current_query_node, query_db);
      VertexCode target_code = new VertexCode();
      target_code.get_index_from_neo4j(target_node, target_db);
      if (helper.is_super_vertex(query_code, target_code)) {
        matched_target_nodes.add(target_node);
      }
    }
    return matched_target_nodes;
  }

  /**
   * Checks if a query clique is a subclique of a target clique.
   *
   * @param query_clique Clique from the query db.
   * @param target_clique Clique from the target db.
   * @return boolean
   */
  private boolean is_subclique(ArrayList<Node> query_clique, ArrayList<Node> target_clique) {
    CliqueCode query_clique_code = new CliqueCode();
    query_clique_code.get_index_from_neo4j(query_clique);
    CliqueCode target_clique_code = new CliqueCode();
    target_clique_code.get_index_from_neo4j(target_clique);
    if (query_clique_code.number_of_vertices_in_clique
        > target_clique_code.number_of_vertices_in_clique) {
      return false;
    }
    if (query_clique_code.sum_of_vertex_labels > target_clique_code.sum_of_vertex_labels) {
      return false;
    }
    for (String query_key : query_clique_code.degree_list_of_vertices.keySet()) {
      boolean found = false;
      for (String target_key : target_clique_code.degree_list_of_vertices.keySet()) {
        if (target_key.equals(query_key)) {
          DatabaseHelpers helper = new DatabaseHelpers();
          if (helper.is_presequence(query_clique_code.degree_list_of_vertices.get(query_key),
              target_clique_code.degree_list_of_vertices.get(target_key))) {
            found = true;
          }
        }
      }
      if (!found) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if the current query node can be mapped to the current target node in consideration
   * using SNP.
   *
   * @param target_node The target node in consideration.
   * @param query_node The query node in consideration.
   * @param SMQ The list of query nodes in the current mapping.
   * @param SMG The list of target nodes in the current mapping.
   * @param position The position at which the query node is in the SMQ.
   * @return boolean
   */
  private boolean matches(Node target_node, Node query_node, SQBCSequence SMQ, SQBCSequence SMG,
      int position) {
    ArrayList<Integer> snp_query = get_snp_of_node(query_node, query_db, SMQ, position);
    ArrayList<Integer> snp_target = get_snp_of_node(target_node, target_db, SMG, position);
    int m1 = snp_query.remove(snp_query.size() - 1);
    int m2 = snp_target.remove(snp_target.size() - 1);
    if (snp_target.containsAll(snp_query) && m1 <= m2) {
      return true;
    }
    return false;
  }

  /**
   * Computes the SNP of a node.
   *
   * @param node Node of which the SNP is to be computed.
   * @param db GraphDatabaseService object representing the database that the node is part of.
   * @param sequence The corresponding matching sequence (SMQ or SMG).
   * @param position Position of the node in the sequence.
   * @return List of numbers representing the SNP.
   */
  private ArrayList<Integer> get_snp_of_node(Node node, GraphDatabaseService db,
      SQBCSequence sequence, int position) {
    ArrayList<Node> neighbors = helper.get_neighbors(node, db, Direction.BOTH);
    ArrayList<Integer> snp = new ArrayList<>();
    if (sequence.size > 0) {
      for (int index = 0; index < position; index++) {
        try {
          Node matched = sequence.get(index);
          if (neighbors.contains(matched)) {
            snp.add(index);
          }
        } catch (Exception e) {
          e.printStackTrace();
          System.exit(2);
        }
      }
    }
    Collections.sort(snp);
    int matched_neighbors = snp.size();
    int neighbor_size = neighbors.size();
    int unmatched_neighbors = Math.abs(matched_neighbors - neighbor_size);
    snp.add(unmatched_neighbors);
    return snp;
  }

  /**
   * Checks if a solution has been found.
   *
   * @param SMQ List of query nodes in the current mapping.
   * @param SMG List of target nodes in the current mapping.
   * @return boolean
   */
  private boolean check_solution_found(SQBCSequence SMQ, SQBCSequence SMG) {
    if (SMG.size == SMQ.size && SMG.size == query_graph_size) {
      HashMap<Node, Node> solution = new HashMap<>();
      for (int index = 0; index < SMQ.size; index++) {
        solution.put(SMQ.get(index), SMG.get(index));
      }
      boolean exists = false;
      for (HashMap<Node, Node> existing : archive.solutions) {
        if (existing.equals(solution)) {
          exists = true;
        }
      }
      if (!exists) {
        archive.solutions.add(solution);
        if (query_file_path.length() > 0 && target_file_path.length() > 0) {
          File query_file = new File(query_file_path);
          File target_file = new File(target_file_path);
          helper.compare_solution_and_ground_truth(ground_truth_file_path, solution,
              query_file.getName(), target_file.getName());
        }
      }
      return true;
    }
    return false;
  }

  public static void main(String[] args) {
    String query_file_path = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/query/backbones_1EMA.8.sub.grf";
    String target_file_path = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/target/backbones_1EMA.grf";
    String ground_truth_file_path = "/home/pbg4930/Desktop/graph_db/Proteins/Proteins/ground_truth/Proteins.8.gtr";
    FileDatabaseService helper = new FileDatabaseService(
        "/home/pbg4930/Desktop/neo4j-community-3.1.1/data/databases/");
    GraphDatabaseService query_db = helper.create_db(query_file_path);
    GraphDatabaseService target_db = helper.create_db(target_file_path);
    try {
      SQBC matcher = new SQBC();
      matcher.construct_index(query_db);
      matcher.construct_index(target_db);
      matcher.match(query_file_path, target_file_path, ground_truth_file_path, query_db, target_db);
      query_db.shutdown();
      target_db.shutdown();
    } catch (Exception e) {
      System.out.println(e);
      e.printStackTrace();
    }
  }
}
