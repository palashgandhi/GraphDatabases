import java.util.ArrayList;
import java.util.HashMap;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;

/**
 * Class that handles the construction and fetch operations of the clique codes for SQBC.
 *
 * @author Palash Gandhi
 * @date 9/13/17. GraphDatabases
 */
public class CliqueCode {

  ArrayList<Node> clique;
  int number_of_vertices_in_clique;
  long sum_of_vertex_labels;
  HashMap<String, ArrayList<Integer>> degree_list_of_vertices;
  DatabaseHelpers helper = new DatabaseHelpers();

  /**
   * Method that builds the second part of the SQBC index i.e the clique code for the passed clique.
   *
   * @param clique The clique in the form of a list of its nodes.
   */
  public void construct_and_set_index_in_neo4j(ArrayList<Node> clique) {
    this.clique = clique;
    number_of_vertices_in_clique = clique.size();
    compute_sum_of_vertex_labels(clique);
    compute_degree_list_of_vertices(clique);
    set_index_in_neo4j();
  }

  /**
   * Helper method that stores the index information within the node itself as attributes. All
   * indexes are stored as attributes starting with "ignore_index_sqbc".
   */
  private void set_index_in_neo4j() {
    for (Node node : clique) {
      node.setProperty("ignore_index_sqbc_number_of_vertices_in_clique",
          number_of_vertices_in_clique);
      node.setProperty("ignore_index_sqbc_sum_of_vertex_labels", sum_of_vertex_labels);
      for (String neighbor : degree_list_of_vertices.keySet()) {
        ArrayList<Integer> ndl = degree_list_of_vertices.get(neighbor);
        Integer[] ndl_info = ndl.toArray(new Integer[ndl.size()]);
        node.setProperty("ignore_index_sqbc_degree_list_of_vertices_" + neighbor, ndl_info);
      }
    }
  }

  /**
   * Gets the index from a neo4j node. All indexes are stored as attributes starting with
   * "ignore_index_sqbc".
   *
   * @param clique The list of nodes representing the clique for which the index needs to be
   * fetched.
   */
  public void get_index_from_neo4j(ArrayList<Node> clique) {
    sum_of_vertex_labels = 0;
    number_of_vertices_in_clique = 0;
    degree_list_of_vertices = new HashMap<>();
    for (Node node : clique) {
      if (sum_of_vertex_labels == 0 && number_of_vertices_in_clique == 0) {
        number_of_vertices_in_clique = (int) node
            .getProperty("ignore_index_sqbc_number_of_vertices_in_clique");
        sum_of_vertex_labels = (long) node.getProperty("ignore_index_sqbc_sum_of_vertex_labels");
      }
      for (String property : node.getPropertyKeys()) {
        if (property.contains("ignore_index_sqbc_degree_list_of_vertices_")) {
          String neighbor = property.substring(
              property.indexOf("ignore_index_sqbc_degree_list_of_vertices_")
                  + "ignore_index_sqbc_degree_list_of_vertices_".length(), property.length());
          int[] deg_list_info = (int[]) node.getProperty(property);
          ArrayList<Integer> deg_list = new ArrayList<>();
          for (Integer d : deg_list_info) {
            deg_list.add(d);
          }
          degree_list_of_vertices.put(neighbor, deg_list);
        }
      }
    }
  }

  /**
   * Helper method to compute the degree list of vertices as explained in SQBC which is part of the
   * clique code.
   *
   * @param clique The list of nodes representing a clique.
   */
  private void compute_degree_list_of_vertices(ArrayList<Node> clique) {
    degree_list_of_vertices = new HashMap();
    for (Node node : clique) {
      for (String label : helper.get_labels(node)) {
        if (!degree_list_of_vertices.containsKey(label)) {
          degree_list_of_vertices.put(label, new ArrayList());
        }
        ArrayList<Integer> degrees = degree_list_of_vertices.get(label);
        degrees.add(node.getDegree());
        degree_list_of_vertices.put(label, degrees);
      }
    }
  }

  /**
   * Helper method to compute the sum of the vertex labels of each node in the clique. It computes
   * the hashcodes of each label and adds it to the sum.
   *
   * @param clique The list of nodes representing a clique.
   */
  private void compute_sum_of_vertex_labels(ArrayList<Node> clique) {
    long sum = 0;
    for (Node node : clique) {
      for (Label label : node.getLabels()) {
        sum += label.name().hashCode();
      }
    }
    sum_of_vertex_labels = sum;
  }

  /**
   * Override toString method for debugging.
   *
   * @return toString custom string.
   */
  public String toString() {
    return " [ " + number_of_vertices_in_clique + ", " + sum_of_vertex_labels + ", " + clique
        .toString() + ", " + degree_list_of_vertices.toString() + " ] ";
  }
}
